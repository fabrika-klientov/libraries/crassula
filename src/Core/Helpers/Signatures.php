<?php
/**
 *
 * @package   Crassula
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.29
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Core\Helpers;

use Crassula\Exceptions\CrassulaException;

trait Signatures
{
    /**
     * @param array $data
     * @param string $secretKey
     * @return string
     * @throws CrassulaException
     */
    protected static function doSignature(array $data, string $secretKey): string
    {
        if (empty($secretKey)) {
            throw new CrassulaException('SecretKey is not be empty');
        }

        return hash_hmac('md5', join(';', self::_signatureOptData($data)), $secretKey);
    }

    /**
     * @param array $data
     * @return string[]
     * @throws CrassulaException
     */
    private static function _signatureOptData(array $data): array
    {
        $optData = [];
        foreach ($data as $item) {
            if (!isset($item)) {
                throw new CrassulaException('All params of signature aren\'t be NULL');
            }

            if (is_array($item)) {
                $optData = array_merge($optData, self::_signatureOptData($item));
                continue;
            }

            $optData[] = $item;
        }

        return $optData;
    }
}
