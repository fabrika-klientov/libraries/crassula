<?php
/**
 *
 * @package   Crassula
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.22
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Core\Http;

use Crassula\Contracts\BeEntity;
use Crassula\Exceptions\CrassulaException;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class HttpClient
{
    use Logging;

    public const API_ENTRY = 'https://api.wayforpay.com/api';
    public const API_MMS_ENTRY = 'https://api.wayforpay.com/mms';
    public const VERIFY_ENTRY = 'https://secure.wayforpay.com/verify';
    public const PAY_ENTRY = 'https://secure.wayforpay.com/pay';

    private $currentEntry;

    /**
     * @param string|null $baseEntry
     */
    public function __construct(string $baseEntry = null)
    {
        self::setCurrentEntry($baseEntry ?? self::API_ENTRY);
    }

    /**
     * @param string $currentEntry
     * @return HttpClient
     */
    public function setCurrentEntry(string $currentEntry): self
    {
        $this->currentEntry = preg_replace('/\/$/', '', $currentEntry);

        return $this;
    }

    /**
     * @return Client
     */
    public function getGuzzleClient(): Client
    {
        return new Client(['base_uri' => $this->currentEntry]);
    }


    /** public GET method request
     * @param string $link
     * @param array $query
     * @param array $guzzleOptions
     * @param bool $isResponseInterface
     * @return ResponseInterface|string|null
     * @throws CrassulaException
     */
    public function get(
        string $link,
        array $query = [],
        array $guzzleOptions = [],
        bool $isResponseInterface = false
    ) {
        $response = $this->request('GET', $link, array_merge($guzzleOptions, ['query' => $query]));

        return $isResponseInterface ? $response : $this->getContents($response);
    }

    /** public POST method request
     * @param string $link
     * @param array $data
     * @param array $guzzleOptions
     * @param bool $isResponseInterface
     * @return ResponseInterface|string|null
     * @throws CrassulaException
     */
    public function post(
        string $link,
        array $data,
        array $guzzleOptions = [],
        bool $isResponseInterface = false
    ) {
        $response = $this->request(
            'POST',
            $link,
            array_merge($guzzleOptions, empty($data) ? [] : ['json' => $data])
        );

        return $isResponseInterface ? $response : $this->getContents($response);
    }

    /** public PATCH method request
     * @param string $link
     * @param array $data
     * @param array $guzzleOptions
     * @param bool $isResponseInterface
     * @return ResponseInterface|string|null
     * @throws CrassulaException
     */
    public function patch(
        string $link,
        array $data,
        array $guzzleOptions = [],
        bool $isResponseInterface = false
    ) {
        $response = $this->request(
            'PATCH',
            $link,
            array_merge($guzzleOptions, empty($data) ? [] : ['json' => $data])
        );

        return $isResponseInterface ? $response : $this->getContents($response);
    }

    /** public DELETE method request
     * @param string $link
     * @param array $query
     * @param array $guzzleOptions
     * @param bool $isResponseInterface
     * @return ResponseInterface|string|null
     * @throws CrassulaException
     */
    public function delete(
        string $link,
        array $query = [],
        array $guzzleOptions = [],
        bool $isResponseInterface = false
    ) {
        $response = $this->request('DELETE', $link, array_merge($guzzleOptions, ['query' => $query]));

        return $isResponseInterface ? $response : $this->getContents($response);
    }

    /** get full url
     * @param string $link
     * @return string
     */
    public function getUrl(string $link): string
    {
        if (empty($link)) {
            return $this->currentEntry;
        }

        if (preg_match('/^https?:\/\/.*/', $link)) {
            return $link;
        }

        $link = preg_replace('/^\//', '', $link);

        return $this->currentEntry . '/' . $link;
    }


    /** smart request
     * @param string $method
     * @param string $link
     * @param array $options
     * @return ResponseInterface
     * @throws CrassulaException
     */
    protected function request(string $method, string $link, array $options = []): ResponseInterface
    {
        $startTime = time();
        $url = $this->getUrl($link);

        try {
            $response = self::getGuzzleClient()->request($method, $url, $options);

            $this->loggingSuccess($method, $url, $options, $response, $startTime);

            $response->getBody()->rewind();

            return $response;
        } catch (\Throwable $throwable) {
            $this->loggingError($method, $url, $options, $throwable, $startTime);

            throw new CrassulaException($throwable->getMessage(), $throwable->getCode(), $throwable);
        }
    }

    /** helper for get body data
     * @param ResponseInterface|null $response
     * @return string|null
     */
    protected function getContents(ResponseInterface $response = null): ?string
    {
        return isset($response) ? $response->getBody()->getContents() : null;
    }

    /**
     * @param mixed $data
     * @param string|null $class
     * @return array|BeEntity
     * @throws CrassulaException
     */
    public static function decodeContent($data, string $class = null)
    {
        if (empty($data)) {
            throw new CrassulaException('Param data is empty');
        }

        if (is_object($data) && $data instanceof ResponseInterface) {
            $data = $data->getBody()->getContents();
        }

        if (is_string($data)) {
            $data = json_decode($data, true);
        }

        if (!is_array($data)) {
            throw new CrassulaException('Unresolved param data');
        }

        if (isset($class) && class_exists($class) && is_subclass_of($class, BeEntity::class)) {
            return new $class($data);
        }

        return $data;
    }
}
