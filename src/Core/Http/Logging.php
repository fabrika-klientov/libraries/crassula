<?php
/**
 *
 * @package   Crassula
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.22
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Core\Http;

use Psr\Http\Message\ResponseInterface;
use Throwable;

trait Logging
{
    protected $logging = false;

    public function withLogging(bool $logging)
    {
        $this->logging = $logging;

        return $this;
    }

    /**
     * @param string $method
     * @param string $link
     * @param array $options
     * @param ResponseInterface $response
     * @param int $startTime
     */
    protected function loggingSuccess(
        string $method,
        string $link,
        array $options,
        ResponseInterface $response,
        int $startTime
    ) {
        if (!$this->logging) {
            return;
        }

        if ($logger = self::getLogger()) {
            $resData = $this->getContents($response);
            $logger->info(
                'CRASSULA:: SUCCESS:: CODE [200] METHOD [' . $method . '] || URL [' . $link . ']',
                [
                    'lib' => 'crassula',
                    'request' => (string)json_encode($options, JSON_UNESCAPED_UNICODE),
                    'response' => substr((string)json_encode($resData ?? '', JSON_UNESCAPED_UNICODE), 0, 10000),
                    'response_code' => $response->getStatusCode(),
                    'execute_time' => time() - $startTime,
                ]
            );
        }
    }

    /**
     * @param string $method
     * @param string $link
     * @param array $options
     * @param Throwable $clientException
     * @param int $startTime
     */
    protected function loggingError(
        string $method,
        string $link,
        array $options,
        Throwable $clientException,
        int $startTime
    ) {
        if (!$this->logging) {
            return;
        }

        if ($logger = self::getLogger()) {
            if (method_exists($clientException, 'getResponse')) {
                $response = $clientException->getResponse();
                $resData = $this->getContents($response);
            }

            $logger->warning(
                'CRASSULA:: ERROR:: CODE [' . $clientException->getCode() . '] METHOD [' . $method . '] || URL [' . $link . ']',
                [
                    'lib' => 'crassula',
                    'request' => (string)json_encode($options, JSON_UNESCAPED_UNICODE),
                    'response' => substr((string)json_encode($resData ?? '', JSON_UNESCAPED_UNICODE), 0, 10000),
                    'response_code' => isset($response) ? $response->getStatusCode() : 'null',
                    'execute_time' => time() - $startTime,
                ]
            );
        }
    }

    private function getLogger()
    {
        if (function_exists('logger')) { // laravel logger
            return logger();
        }

        return null;
    }
}
