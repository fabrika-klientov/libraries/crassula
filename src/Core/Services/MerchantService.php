<?php
/**
 *
 * @package   Crassula
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.07.28
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Core\Services;

use Crassula\Contracts\BeService;
use Crassula\Core\Http\HttpClient;
use Crassula\Entities\Charge;
use Crassula\Entities\ChargeResult;
use Crassula\Entities\CheckStatus;
use Crassula\Entities\CheckStatusResult;
use Crassula\Entities\CreateMerchant;
use Crassula\Entities\CreateMerchantResult;
use Crassula\Entities\MerchantInfoResult;
use Crassula\Entities\Purchase;
use Crassula\Entities\PurchaseResult;
use Crassula\Entities\Refund;
use Crassula\Entities\RefundResult;
use Crassula\Entities\Settle;
use Crassula\Entities\SettleResult;
use Crassula\Exceptions\CrassulaException;

/**
 * @deprecated
 * */
class MerchantService extends BaseService implements BeService
{
    use Predefined;

    public const MERCHANT_INFO = 'merchantInfo.php';
    public const MERCHANT_ADD = 'addMerchant.php';
    public const MERCHANT_EDIT = 'merchantEdit.php';
    public const MERCHANT_BALANCE = 'merchantBalance.php';

    /**
     * @deprecated
     * @return PurchaseResult
     * @throws CrassulaException
     */
    public function merchantInfo()
    {
        $result = $this->client
            ->getHttpClient()
            ->setCurrentEntry(HttpClient::API_MMS_ENTRY)
            ->post(
                self::MERCHANT_INFO,
                [
                    'merchantAccount' => $this->client->getConfig()->getMerchantAccount(),
                    'merchantSignature' => $this->client->signature([$this->client->getConfig()->getMerchantAccount()]),
                ]
            );

        return HttpClient::decodeContent($result, MerchantInfoResult::class);
    }

    /**
     * @deprecated
     * @param CreateMerchant $merchant
     * @return CreateMerchantResult
     * @throws CrassulaException
     */
    public function store(CreateMerchant $merchant)
    {
        $this->injectPredefinedDataToEntity($merchant, $this->client->getConfig());

        $result = $this->client
            ->getHttpClient()
            ->setCurrentEntry(HttpClient::API_MMS_ENTRY)
            ->post(
                self::MERCHANT_ADD,
                array_merge(
                    $merchant->jsonData(),
                    ['merchantSignature' => $merchant->signature($this->client->getConfig()->getMerchantSecretKey())]
                )
            );

        return HttpClient::decodeContent($result, CreateMerchantResult::class);
    }

    /**
     * @deprecated
     * @param $merchant
     * @return mixed
     */
    public function update($merchant)
    {
        // TODO: add
    }

    /**
     * @deprecated
     * @param $merchant
     * @return mixed
     */
    public function balance($merchant)
    {
        // TODO: add
    }
}
