<?php
/**
 *
 * @package   Crassula
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.23
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Core\Services;

use Crassula\Client;
use Crassula\Contracts\BeEntity;
use Crassula\Contracts\BeRequired;

abstract class BaseService
{
    protected $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @param BeEntity $entity
     * @throws \Crassula\Exceptions\CrassulaException
     */
    protected function controlRequire(BeEntity $entity): void
    {
        if ($entity instanceof BeRequired) {
            $entity->controlRequire();
        }
    }
}
