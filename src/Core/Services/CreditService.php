<?php
/**
 *
 * @package   Crassula
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.23
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Core\Services;

use Crassula\Contracts\BeService;
use Crassula\Core\Http\HttpClient;
use Crassula\Entities\CreditCard;
use Crassula\Entities\CreditCardResult;

class CreditService extends BaseService implements BeService
{
    use Predefined;

    /**
     * @param CreditCard $creditCard
     * @return CreditCardResult
     * @throws \Crassula\Exceptions\CrassulaException
     */
    public function credit(CreditCard $creditCard)
    {
        $this->injectPredefinedDataToEntity($creditCard, $this->client->getConfig());
        $this->controlRequire($creditCard);

        $result = $this->client
            ->getHttpClient()
            ->setCurrentEntry(HttpClient::API_ENTRY)
            ->post(
                '',
                array_merge(
                    $creditCard->jsonData(),
                    [
                        'merchantSignature' => $creditCard
                            ->signature($this->client->getConfig()->getMerchantSecretKey()),
                    ]
                )
            );

        return HttpClient::decodeContent($result, CreditCardResult::class);
    }
}
