<?php
/**
 *
 * @package   Crassula
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.23
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Core\Services;

use Crassula\Contracts\BeService;
use Crassula\Core\Http\HttpClient;
use Crassula\Entities\TransactionRequest;
use Crassula\Entities\TransactionResult;

class TransactionService extends BaseService implements BeService
{
    use Predefined;

    /**
     * @param TransactionRequest $transactionRequest
     * @return TransactionResult
     * @throws \Crassula\Exceptions\CrassulaException
     */
    public function list(TransactionRequest $transactionRequest)
    {
        $this->injectPredefinedDataToEntity($transactionRequest, $this->client->getConfig());
        $this->controlRequire($transactionRequest);

        $result = $this->client
            ->getHttpClient()
            ->setCurrentEntry(HttpClient::API_ENTRY)
            ->post(
                '',
                array_merge(
                    $transactionRequest->jsonData(),
                    [
                        'merchantSignature' => $transactionRequest
                            ->signature($this->client->getConfig()->getMerchantSecretKey()),
                    ]
                )
            );

        return HttpClient::decodeContent($result, TransactionResult::class);
    }
}
