<?php
/**
 *
 * @package   Crassula
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.23
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Core\Services;

use Crassula\Contracts\BeEntity;
use Crassula\Contracts\BePredefined;
use Crassula\Entities\CrassulaConfig;

trait Predefined
{
    /**
     * @param BeEntity $entity
     * @param CrassulaConfig $config
     * @param bool $force
     */
    protected function injectPredefinedDataToEntity(BeEntity $entity, CrassulaConfig $config, bool $force = false): void
    {
        if ($entity instanceof BePredefined) {
            $entity->predefine($config, $force);
        }
    }
}
