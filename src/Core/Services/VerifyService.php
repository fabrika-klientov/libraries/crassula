<?php
/**
 *
 * @package   Crassula
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.23
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Core\Services;

use Crassula\Contracts\BeService;
use Crassula\Core\Http\HttpClient;
use Crassula\Entities\VerifyCardGetToken;
use Crassula\Entities\VerifyCardGetTokenResult;
use Crassula\Entities\VerifyPayPage;

class VerifyService extends BaseService implements BeService
{
    use Predefined;

    /**
     * @param VerifyPayPage $verifyPayPage
     * @return string|null
     * @throws \Crassula\Exceptions\CrassulaException
     */
    public function verifyInPayPage(VerifyPayPage $verifyPayPage)
    {
        $this->injectPredefinedDataToEntity($verifyPayPage, $this->client->getConfig());

        $result = $this->client
            ->getHttpClient()
            ->setCurrentEntry(HttpClient::VERIFY_ENTRY)
            ->post(
                '',
                array_merge(
                    $verifyPayPage->jsonData(),
                    [
                        'merchantSignature' => $verifyPayPage->signature($this->client->getConfig()->getMerchantSecretKey()),
                    ]
                )
            );

        return $result;
    }

    /**
     * @param VerifyCardGetToken $verifyCardGetToken
     * @return VerifyCardGetTokenResult
     * @throws \Crassula\Exceptions\CrassulaException
     */
    public function verifyCardGetToken(VerifyCardGetToken $verifyCardGetToken)
    {
        $this->injectPredefinedDataToEntity($verifyCardGetToken, $this->client->getConfig());
        $this->controlRequire($verifyCardGetToken);

        $result = $this->client
            ->getHttpClient()
            ->setCurrentEntry(HttpClient::API_ENTRY)
            ->post(
                '',
                array_merge(
                    $verifyCardGetToken->jsonData(),
                    [
                        'merchantSignature' => $verifyCardGetToken->signature($this->client->getConfig()->getMerchantSecretKey()),
                    ]
                )
            );

        return HttpClient::decodeContent($result, VerifyCardGetTokenResult::class);
    }
}
