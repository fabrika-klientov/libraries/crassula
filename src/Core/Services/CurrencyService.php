<?php
/**
 *
 * @package   Crassula
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.23
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Core\Services;

use Crassula\Contracts\BeService;
use Crassula\Core\Http\HttpClient;
use Crassula\Entities\Currency;
use Crassula\Entities\CurrencyResult;

class CurrencyService extends BaseService implements BeService
{
    use Predefined;

    /**
     * @param Currency $currency
     * @return CurrencyResult
     * @throws \Crassula\Exceptions\CrassulaException
     */
    public function currencyRates(Currency $currency)
    {
        $this->injectPredefinedDataToEntity($currency, $this->client->getConfig());
        $this->controlRequire($currency);

        $result = $this->client
            ->getHttpClient()
            ->setCurrentEntry(HttpClient::API_ENTRY)
            ->post(
                '',
                array_merge(
                    $currency->jsonData(),
                    ['merchantSignature' => $currency->signature($this->client->getConfig()->getMerchantSecretKey())]
                )
            );

        return HttpClient::decodeContent($result, CurrencyResult::class);
    }
}
