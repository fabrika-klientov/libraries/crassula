<?php
/**
 *
 * @package   Crassula
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.23
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Core\Services;

use Crassula\Contracts\BeService;
use Crassula\Core\Http\HttpClient;
use Crassula\Entities\CheckingAccount;
use Crassula\Entities\CheckingAccountResult;

class CheckingAccountService extends BaseService implements BeService
{
    use Predefined;

    /**
     * @param CheckingAccount $checkingAccount
     * @return CheckingAccountResult
     * @throws \Crassula\Exceptions\CrassulaException
     */
    public function fillAccount(CheckingAccount $checkingAccount)
    {
        $this->injectPredefinedDataToEntity($checkingAccount, $this->client->getConfig());
        $this->controlRequire($checkingAccount);

        $result = $this->client
            ->getHttpClient()
            ->setCurrentEntry(HttpClient::API_ENTRY)
            ->post(
                '',
                array_merge(
                    $checkingAccount->jsonData(),
                    [
                        'merchantSignature' => $checkingAccount
                            ->signature($this->client->getConfig()->getMerchantSecretKey()),
                    ]
                )
            );

        return HttpClient::decodeContent($result, CheckingAccountResult::class);
    }
}
