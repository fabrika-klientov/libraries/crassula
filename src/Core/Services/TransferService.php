<?php
/**
 *
 * @package   Crassula
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.23
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Core\Services;

use Crassula\Contracts\BeService;
use Crassula\Core\Http\HttpClient;
use Crassula\Entities\TransferCardToCard;
use Crassula\Entities\TransferCardToCardResult;

class TransferService extends BaseService implements BeService
{
    use Predefined;

    /**
     * @param TransferCardToCard $transferCardToCard
     * @return TransferCardToCardResult
     * @throws \Crassula\Exceptions\CrassulaException
     */
    public function transfer(TransferCardToCard $transferCardToCard)
    {
        $this->injectPredefinedDataToEntity($transferCardToCard, $this->client->getConfig());
        $this->controlRequire($transferCardToCard);

        $result = $this->client
            ->getHttpClient()
            ->setCurrentEntry(HttpClient::API_ENTRY)
            ->post(
                '',
                array_merge(
                    $transferCardToCard->jsonData(),
                    [
                        'merchantSignature' => $transferCardToCard
                            ->signature($this->client->getConfig()->getMerchantSecretKey()),
                    ]
                )
            );

        return HttpClient::decodeContent($result, TransferCardToCardResult::class);
    }
}
