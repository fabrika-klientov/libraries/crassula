<?php
/**
 *
 * @package   Crassula
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.23
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Core\Services;

use Crassula\Contracts\BeService;
use Crassula\Core\Http\HttpClient;
use Crassula\Entities\CreateInvoice;
use Crassula\Entities\CreateInvoiceResult;
use Crassula\Entities\RemoveInvoice;
use Crassula\Entities\RemoveInvoiceResult;

class InvoiceService extends BaseService implements BeService
{
    use Predefined;

    /**
     * @param CreateInvoice $createInvoice
     * @return CreateInvoiceResult
     * @throws \Crassula\Exceptions\CrassulaException
     */
    public function create(CreateInvoice $createInvoice)
    {
        $this->injectPredefinedDataToEntity($createInvoice, $this->client->getConfig());
        $this->controlRequire($createInvoice);

        $result = $this->client
            ->getHttpClient()
            ->setCurrentEntry(HttpClient::API_ENTRY)
            ->post(
                '',
                array_merge(
                    $createInvoice->jsonData(),
                    [
                        'merchantSignature' => $createInvoice
                            ->signature($this->client->getConfig()->getMerchantSecretKey()),
                    ]
                )
            );

        return HttpClient::decodeContent($result, CreateInvoiceResult::class);
    }

    /**
     * @param RemoveInvoice $removeInvoice
     * @return RemoveInvoiceResult
     * @throws \Crassula\Exceptions\CrassulaException
     */
    public function remove(RemoveInvoice $removeInvoice)
    {
        $this->injectPredefinedDataToEntity($removeInvoice, $this->client->getConfig());

        $result = $this->client
            ->getHttpClient()
            ->setCurrentEntry(HttpClient::API_ENTRY)
            ->post(
                '',
                array_merge(
                    $removeInvoice->jsonData(),
                    [
                        'merchantSignature' => $removeInvoice
                            ->signature($this->client->getConfig()->getMerchantSecretKey()),
                    ]
                )
            );

        return HttpClient::decodeContent($result, RemoveInvoiceResult::class);
    }
}
