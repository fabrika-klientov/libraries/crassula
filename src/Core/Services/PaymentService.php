<?php
/**
 *
 * @package   Crassula
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.23
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Core\Services;

use Crassula\Contracts\BeService;
use Crassula\Core\Http\HttpClient;
use Crassula\Entities\Charge;
use Crassula\Entities\ChargeResult;
use Crassula\Entities\CheckStatus;
use Crassula\Entities\CheckStatusResult;
use Crassula\Entities\Purchase;
use Crassula\Entities\PurchaseResult;
use Crassula\Entities\Refund;
use Crassula\Entities\RefundResult;
use Crassula\Entities\Settle;
use Crassula\Entities\SettleResult;
use Crassula\Exceptions\CrassulaException;

class PaymentService extends BaseService implements BeService
{
    use Predefined;

    /**
     * @param Purchase $purchase
     * @param bool $offline
     * @return PurchaseResult|string|null
     * @throws CrassulaException
     */
    public function purchase(Purchase $purchase, bool $offline = true)
    {
        $this->injectPredefinedDataToEntity($purchase, $this->client->getConfig());
        $this->controlRequire($purchase);

        $result = $this->client
            ->getHttpClient()
            ->setCurrentEntry(HttpClient::PAY_ENTRY)
            ->post(
                '',
                array_merge(
                    $purchase->jsonData(),
                    ['merchantSignature' => $purchase->signature($this->client->getConfig()->getMerchantSecretKey())]
                ),
                $offline ? ['query' => ['behavior' => 'offline']] : []
            );

        if ($offline) {
            return HttpClient::decodeContent($result, PurchaseResult::class);
        }

        return $result;
    }

    /**
     * @param Settle $settle
     * @return SettleResult
     * @throws CrassulaException
     */
    public function settle(Settle $settle)
    {
        $this->injectPredefinedDataToEntity($settle, $this->client->getConfig());

        $result = $this->client
            ->getHttpClient()
            ->setCurrentEntry(HttpClient::API_ENTRY)
            ->post(
                '',
                array_merge(
                    $settle->jsonData(),
                    ['merchantSignature' => $settle->signature($this->client->getConfig()->getMerchantSecretKey())]
                )
            );

        return HttpClient::decodeContent($result, SettleResult::class);
    }

    /**
     * @param Charge $charge
     * @return ChargeResult
     * @throws CrassulaException
     */
    public function charge(Charge $charge)
    {
        $this->injectPredefinedDataToEntity($charge, $this->client->getConfig());

        $result = $this->client
            ->getHttpClient()
            ->setCurrentEntry(HttpClient::API_ENTRY)
            ->post(
                '',
                array_merge(
                    $charge->jsonData(),
                    ['merchantSignature' => $charge->signature($this->client->getConfig()->getMerchantSecretKey())]
                )
            );

        return HttpClient::decodeContent($result, ChargeResult::class);
    }

    /**
     * @param CheckStatus $checkStatus
     * @return CheckStatusResult
     * @throws CrassulaException
     */
    public function checkStatus(CheckStatus $checkStatus)
    {
        $this->injectPredefinedDataToEntity($checkStatus, $this->client->getConfig());

        $result = $this->client
            ->getHttpClient()
            ->setCurrentEntry(HttpClient::API_ENTRY)
            ->post(
                '',
                array_merge(
                    $checkStatus->jsonData(),
                    ['merchantSignature' => $checkStatus->signature($this->client->getConfig()->getMerchantSecretKey())]
                )
            );

        return HttpClient::decodeContent($result, CheckStatusResult::class);
    }

    /**
     * @param Refund $refund
     * @return RefundResult
     * @throws CrassulaException
     */
    public function refund(Refund $refund)
    {
        $this->injectPredefinedDataToEntity($refund, $this->client->getConfig());

        $result = $this->client
            ->getHttpClient()
            ->setCurrentEntry(HttpClient::API_ENTRY)
            ->post(
                '',
                array_merge(
                    $refund->jsonData(),
                    ['merchantSignature' => $refund->signature($this->client->getConfig()->getMerchantSecretKey())]
                )
            );

        return HttpClient::decodeContent($result, RefundResult::class);
    }
}
