<?php
/**
 *
 * @package   Crassula
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.23
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Entities;

use Crassula\Contracts\BeEntity;

class CrassulaConfig extends Entity implements BeEntity
{
    protected $merchantAccount;
    protected $merchantSecretKey;
    protected $merchantDomainName;

    /**
     * @return string
     */
    public function getMerchantAccount()
    {
        return $this->merchantAccount;
    }

    /**
     * @param string $merchantAccount
     */
    public function setMerchantAccount($merchantAccount): void
    {
        $this->merchantAccount = $merchantAccount;
    }

    /**
     * @return string
     */
    public function getMerchantSecretKey()
    {
        return $this->merchantSecretKey;
    }

    /**
     * @param string $merchantSecretKey
     */
    public function setMerchantSecretKey($merchantSecretKey): void
    {
        $this->merchantSecretKey = $merchantSecretKey;
    }

    /**
     * @return string
     */
    public function getMerchantDomainName()
    {
        return $this->merchantDomainName;
    }

    /**
     * @param string $merchantDomainName
     */
    public function setMerchantDomainName($merchantDomainName): void
    {
        $this->merchantDomainName = $merchantDomainName;
    }
}
