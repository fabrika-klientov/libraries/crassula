<?php
/**
 *
 * @package   Crassula
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.23
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Entities;

use Crassula\Contracts\BeEntity;
use Crassula\Contracts\BeSignature;

class PurchaseResult extends Entity implements BeEntity, BeSignature
{
    protected $merchantAccount;
    protected $orderReference;
    protected $merchantSignature;
    protected $amount;
    protected $currency;
    protected $authCode;
    protected $clientName;
    protected $email;
    protected $phone;
    protected $createdDate;
    protected $processingDate;
    protected $cardPan;
    protected $cardType;
    protected $issuerBankCountry;
    protected $issuerBankName;
    protected $recToken;
    protected $transactionStatus;
    protected $reason;
    protected $reasonCode;
    protected $fee;
    protected $paymentSystem;
    protected $repayUrl;
    protected $url;
    protected $products;

    /**
     * @return mixed
     */
    public function getMerchantAccount()
    {
        return $this->merchantAccount;
    }

    /**
     * @param mixed $merchantAccount
     */
    public function setMerchantAccount($merchantAccount): void
    {
        $this->merchantAccount = $merchantAccount;
    }

    /**
     * @return mixed
     */
    public function getOrderReference()
    {
        return $this->orderReference;
    }

    /**
     * @param mixed $orderReference
     */
    public function setOrderReference($orderReference): void
    {
        $this->orderReference = $orderReference;
    }

    /**
     * @return mixed
     */
    public function getMerchantSignature()
    {
        return $this->merchantSignature;
    }

    /**
     * @param mixed $merchantSignature
     */
    public function setMerchantSignature($merchantSignature): void
    {
        $this->merchantSignature = $merchantSignature;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getAuthCode()
    {
        return $this->authCode;
    }

    /**
     * @param mixed $authCode
     */
    public function setAuthCode($authCode): void
    {
        $this->authCode = $authCode;
    }

    /**
     * @return mixed
     */
    public function getClientName()
    {
        return $this->clientName;
    }

    /**
     * @param mixed $clientName
     */
    public function setClientName($clientName): void
    {
        $this->clientName = $clientName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param mixed $createdDate
     */
    public function setCreatedDate($createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return mixed
     */
    public function getProcessingDate()
    {
        return $this->processingDate;
    }

    /**
     * @param mixed $processingDate
     */
    public function setProcessingDate($processingDate): void
    {
        $this->processingDate = $processingDate;
    }

    /**
     * @return mixed
     */
    public function getCardPan()
    {
        return $this->cardPan;
    }

    /**
     * @param mixed $cardPan
     */
    public function setCardPan($cardPan): void
    {
        $this->cardPan = $cardPan;
    }

    /**
     * @return mixed
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    /**
     * @param mixed $cardType
     */
    public function setCardType($cardType): void
    {
        $this->cardType = $cardType;
    }

    /**
     * @return mixed
     */
    public function getIssuerBankCountry()
    {
        return $this->issuerBankCountry;
    }

    /**
     * @param mixed $issuerBankCountry
     */
    public function setIssuerBankCountry($issuerBankCountry): void
    {
        $this->issuerBankCountry = $issuerBankCountry;
    }

    /**
     * @return mixed
     */
    public function getIssuerBankName()
    {
        return $this->issuerBankName;
    }

    /**
     * @param mixed $issuerBankName
     */
    public function setIssuerBankName($issuerBankName): void
    {
        $this->issuerBankName = $issuerBankName;
    }

    /**
     * @return mixed
     */
    public function getRecToken()
    {
        return $this->recToken;
    }

    /**
     * @param mixed $recToken
     */
    public function setRecToken($recToken): void
    {
        $this->recToken = $recToken;
    }

    /**
     * @return mixed
     */
    public function getTransactionStatus()
    {
        return $this->transactionStatus;
    }

    /**
     * @param mixed $transactionStatus
     */
    public function setTransactionStatus($transactionStatus): void
    {
        $this->transactionStatus = $transactionStatus;
    }

    /**
     * @return mixed
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param mixed $reason
     */
    public function setReason($reason): void
    {
        $this->reason = $reason;
    }

    /**
     * @return mixed
     */
    public function getReasonCode()
    {
        return $this->reasonCode;
    }

    /**
     * @param mixed $reasonCode
     */
    public function setReasonCode($reasonCode): void
    {
        $this->reasonCode = $reasonCode;
    }

    /**
     * @return mixed
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * @param mixed $fee
     */
    public function setFee($fee): void
    {
        $this->fee = $fee;
    }

    /**
     * @return mixed
     */
    public function getPaymentSystem()
    {
        return $this->paymentSystem;
    }

    /**
     * @param mixed $paymentSystem
     */
    public function setPaymentSystem($paymentSystem): void
    {
        $this->paymentSystem = $paymentSystem;
    }

    /**
     * @return mixed
     */
    public function getRepayUrl()
    {
        return $this->repayUrl;
    }

    /**
     * @param mixed $repayUrl
     */
    public function setRepayUrl($repayUrl): void
    {
        $this->repayUrl = $repayUrl;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     * ["name": "Free", "price": 0, "count": 1]
     * @return array
     */
    public function getProducts()
    {
        return $this->products ?? [];
    }

    /**
     * @param array $products
     */
    public function setProducts(array $products): void
    {
        $this->products = $products;
    }

    public function signature(string $secretKey): string
    {
        return self::doSignature(
            [
                $this->getMerchantAccount(),
                $this->getOrderReference(),
                $this->getAmount(),
                $this->getCurrency(),
                $this->getAuthCode() ?: '',
                $this->getCardPan() ?: '',
                $this->getTransactionStatus(),
                $this->getReasonCode(),
            ],
            $secretKey
        );
    }
}
