<?php
/**
 *
 * @package   Crassula
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.22
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Entities;

use Crassula\Contracts\BeEntity;
use Crassula\Contracts\BePredefined;
use Crassula\Contracts\BeRequired;
use Crassula\Contracts\BeSignature;

class Purchase extends Entity implements BeEntity, BeSignature, BePredefined, BeRequired
{
    protected $merchantAccount;
    protected $merchantAuthType;
    protected $merchantDomainName;
    protected $merchantTransactionType;
    protected $merchantTransactionSecureType;
    protected $merchantSignature;
    protected $apiVersion;
    protected $language;
    protected $returnUrl;
    protected $serviceUrl;
    protected $orderReference;
    protected $orderDate;
    protected $orderNo;
    protected $amount;
    protected $currency;
    protected $alternativeAmount;
    protected $alternativeCurrency;
    protected $holdTimeout;
    protected $orderTimeout;
    protected $orderLifetime;
    protected $recToken;
    protected $productName;
    protected $productPrice;
    protected $productCount;
    protected $clientAccountId;
    protected $socialUri;
    protected $deliveryList;
    protected $clientFirstName;
    protected $clientLastName;
    protected $clientAddress;
    protected $clientCity;
    protected $clientState;
    protected $clientZipCode;
    protected $clientCountry;
    protected $clientEmail;
    protected $clientPhone;
    protected $deliveryFirstName;
    protected $deliveryLastName;
    protected $deliveryAddress;
    protected $deliveryCity;
    protected $deliveryState;
    protected $deliveryZipCode;
    protected $deliveryCountry;
    protected $deliveryEmail;
    protected $deliveryPhone;
    protected $aviaDepartureDate;
    protected $aviaLocationNumber;
    protected $aviaLocationCodes;
    protected $aviaFirstName;
    protected $aviaLastName;
    protected $aviaReservationCode;
    protected $regularMode;
    protected $regularAmount;
    protected $dateNext;
    protected $dateEnd;
    protected $regularCount;
    protected $regularOn;
    protected $paymentSystems;
    protected $defaultPaymentSystem;

    protected $__propsRequired = [
        'orderReference',
        'amount',
        'currency',
        'products',
        'orderDate',
        'merchantDomainName',
    ];

    /**
     * @return mixed
     */
    public function getMerchantAccount()
    {
        return $this->merchantAccount;
    }

    /**
     * @param mixed $merchantAccount
     */
    public function setMerchantAccount($merchantAccount): void
    {
        $this->merchantAccount = $merchantAccount;
    }

    /**
     * @return mixed
     */
    public function getMerchantAuthType()
    {
        return $this->merchantAuthType;
    }

    /**
     * @param mixed $merchantAuthType
     */
    public function setMerchantAuthType($merchantAuthType): void
    {
        $this->merchantAuthType = $merchantAuthType;
    }

    /**
     * @return mixed
     */
    public function getMerchantDomainName()
    {
        return $this->merchantDomainName;
    }

    /**
     * @param mixed $merchantDomainName
     */
    public function setMerchantDomainName($merchantDomainName): void
    {
        $this->merchantDomainName = $merchantDomainName;
    }

    /**
     * @return mixed
     */
    public function getMerchantTransactionType()
    {
        return $this->merchantTransactionType;
    }

    /**
     * @param mixed $merchantTransactionType
     */
    public function setMerchantTransactionType($merchantTransactionType): void
    {
        $this->merchantTransactionType = $merchantTransactionType;
    }

    /**
     * @return mixed
     */
    public function getMerchantTransactionSecureType()
    {
        return $this->merchantTransactionSecureType;
    }

    /**
     * @param mixed $merchantTransactionSecureType
     */
    public function setMerchantTransactionSecureType($merchantTransactionSecureType): void
    {
        $this->merchantTransactionSecureType = $merchantTransactionSecureType;
    }

    /**
     * @return mixed
     */
    public function getMerchantSignature()
    {
        return $this->merchantSignature;
    }

    /**
     * @param mixed $merchantSignature
     */
    public function setMerchantSignature($merchantSignature): void
    {
        $this->merchantSignature = $merchantSignature;
    }

    /**
     * @return mixed
     */
    public function getApiVersion()
    {
        return $this->apiVersion;
    }

    /**
     * @param mixed $apiVersion
     */
    public function setApiVersion($apiVersion): void
    {
        $this->apiVersion = $apiVersion;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language): void
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getReturnUrl()
    {
        return $this->returnUrl;
    }

    /**
     * @param mixed $returnUrl
     */
    public function setReturnUrl($returnUrl): void
    {
        $this->returnUrl = $returnUrl;
    }

    /**
     * @return mixed
     */
    public function getServiceUrl()
    {
        return $this->serviceUrl;
    }

    /**
     * @param mixed $serviceUrl
     */
    public function setServiceUrl($serviceUrl): void
    {
        $this->serviceUrl = $serviceUrl;
    }

    /**
     * @return mixed
     */
    public function getOrderReference()
    {
        return $this->orderReference;
    }

    /**
     * @param mixed $orderReference
     */
    public function setOrderReference($orderReference): void
    {
        $this->orderReference = $orderReference;
    }

    /**
     * @return mixed
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * @param mixed $orderDate
     */
    public function setOrderDate($orderDate): void
    {
        $this->orderDate = $orderDate;
    }

    /**
     * @return mixed
     */
    public function getOrderNo()
    {
        return $this->orderNo;
    }

    /**
     * @param mixed $orderNo
     */
    public function setOrderNo($orderNo): void
    {
        $this->orderNo = $orderNo;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getAlternativeAmount()
    {
        return $this->alternativeAmount;
    }

    /**
     * @param mixed $alternativeAmount
     */
    public function setAlternativeAmount($alternativeAmount): void
    {
        $this->alternativeAmount = $alternativeAmount;
    }

    /**
     * @return mixed
     */
    public function getAlternativeCurrency()
    {
        return $this->alternativeCurrency;
    }

    /**
     * @param mixed $alternativeCurrency
     */
    public function setAlternativeCurrency($alternativeCurrency): void
    {
        $this->alternativeCurrency = $alternativeCurrency;
    }

    /**
     * @return mixed
     */
    public function getHoldTimeout()
    {
        return $this->holdTimeout;
    }

    /**
     * @param mixed $holdTimeout
     */
    public function setHoldTimeout($holdTimeout): void
    {
        $this->holdTimeout = $holdTimeout;
    }

    /**
     * @return mixed
     */
    public function getOrderTimeout()
    {
        return $this->orderTimeout;
    }

    /**
     * @param mixed $orderTimeout
     */
    public function setOrderTimeout($orderTimeout): void
    {
        $this->orderTimeout = $orderTimeout;
    }

    /**
     * @return mixed
     */
    public function getOrderLifetime()
    {
        return $this->orderLifetime;
    }

    /**
     * @param mixed $orderLifetime
     */
    public function setOrderLifetime($orderLifetime): void
    {
        $this->orderLifetime = $orderLifetime;
    }

    /**
     * @return mixed
     */
    public function getRecToken()
    {
        return $this->recToken;
    }

    /**
     * @param mixed $recToken
     */
    public function setRecToken($recToken): void
    {
        $this->recToken = $recToken;
    }

    /**
     * @return mixed
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * @param mixed $productName
     */
    public function setProductName($productName): void
    {
        $this->productName = $productName;
    }

    /**
     * @return mixed
     */
    public function getProductPrice()
    {
        return $this->productPrice;
    }

    /**
     * @param mixed $productPrice
     */
    public function setProductPrice($productPrice): void
    {
        $this->productPrice = $productPrice;
    }

    /**
     * @return mixed
     */
    public function getProductCount()
    {
        return $this->productCount;
    }

    /**
     * @param mixed $productCount
     */
    public function setProductCount($productCount): void
    {
        $this->productCount = $productCount;
    }

    /**
     * @return mixed
     */
    public function getClientAccountId()
    {
        return $this->clientAccountId;
    }

    /**
     * @param mixed $clientAccountId
     */
    public function setClientAccountId($clientAccountId): void
    {
        $this->clientAccountId = $clientAccountId;
    }

    /**
     * @return mixed
     */
    public function getSocialUri()
    {
        return $this->socialUri;
    }

    /**
     * @param mixed $socialUri
     */
    public function setSocialUri($socialUri): void
    {
        $this->socialUri = $socialUri;
    }

    /**
     * @return mixed
     */
    public function getDeliveryList()
    {
        return $this->deliveryList;
    }

    /**
     * @param mixed $deliveryList
     */
    public function setDeliveryList($deliveryList): void
    {
        $this->deliveryList = $deliveryList;
    }

    /**
     * @return mixed
     */
    public function getClientFirstName()
    {
        return $this->clientFirstName;
    }

    /**
     * @param mixed $clientFirstName
     */
    public function setClientFirstName($clientFirstName): void
    {
        $this->clientFirstName = $clientFirstName;
    }

    /**
     * @return mixed
     */
    public function getClientLastName()
    {
        return $this->clientLastName;
    }

    /**
     * @param mixed $clientLastName
     */
    public function setClientLastName($clientLastName): void
    {
        $this->clientLastName = $clientLastName;
    }

    /**
     * @return mixed
     */
    public function getClientAddress()
    {
        return $this->clientAddress;
    }

    /**
     * @param mixed $clientAddress
     */
    public function setClientAddress($clientAddress): void
    {
        $this->clientAddress = $clientAddress;
    }

    /**
     * @return mixed
     */
    public function getClientCity()
    {
        return $this->clientCity;
    }

    /**
     * @param mixed $clientCity
     */
    public function setClientCity($clientCity): void
    {
        $this->clientCity = $clientCity;
    }

    /**
     * @return mixed
     */
    public function getClientState()
    {
        return $this->clientState;
    }

    /**
     * @param mixed $clientState
     */
    public function setClientState($clientState): void
    {
        $this->clientState = $clientState;
    }

    /**
     * @return mixed
     */
    public function getClientZipCode()
    {
        return $this->clientZipCode;
    }

    /**
     * @param mixed $clientZipCode
     */
    public function setClientZipCode($clientZipCode): void
    {
        $this->clientZipCode = $clientZipCode;
    }

    /**
     * @return mixed
     */
    public function getClientCountry()
    {
        return $this->clientCountry;
    }

    /**
     * @param mixed $clientCountry
     */
    public function setClientCountry($clientCountry): void
    {
        $this->clientCountry = $clientCountry;
    }

    /**
     * @return mixed
     */
    public function getClientEmail()
    {
        return $this->clientEmail;
    }

    /**
     * @param mixed $clientEmail
     */
    public function setClientEmail($clientEmail): void
    {
        $this->clientEmail = $clientEmail;
    }

    /**
     * @return mixed
     */
    public function getClientPhone()
    {
        return $this->clientPhone;
    }

    /**
     * @param mixed $clientPhone
     */
    public function setClientPhone($clientPhone): void
    {
        $this->clientPhone = $clientPhone;
    }

    /**
     * @return mixed
     */
    public function getDeliveryFirstName()
    {
        return $this->deliveryFirstName;
    }

    /**
     * @param mixed $deliveryFirstName
     */
    public function setDeliveryFirstName($deliveryFirstName): void
    {
        $this->deliveryFirstName = $deliveryFirstName;
    }

    /**
     * @return mixed
     */
    public function getDeliveryLastName()
    {
        return $this->deliveryLastName;
    }

    /**
     * @param mixed $deliveryLastName
     */
    public function setDeliveryLastName($deliveryLastName): void
    {
        $this->deliveryLastName = $deliveryLastName;
    }

    /**
     * @return mixed
     */
    public function getDeliveryAddress()
    {
        return $this->deliveryAddress;
    }

    /**
     * @param mixed $deliveryAddress
     */
    public function setDeliveryAddress($deliveryAddress): void
    {
        $this->deliveryAddress = $deliveryAddress;
    }

    /**
     * @return mixed
     */
    public function getDeliveryCity()
    {
        return $this->deliveryCity;
    }

    /**
     * @param mixed $deliveryCity
     */
    public function setDeliveryCity($deliveryCity): void
    {
        $this->deliveryCity = $deliveryCity;
    }

    /**
     * @return mixed
     */
    public function getDeliveryState()
    {
        return $this->deliveryState;
    }

    /**
     * @param mixed $deliveryState
     */
    public function setDeliveryState($deliveryState): void
    {
        $this->deliveryState = $deliveryState;
    }

    /**
     * @return mixed
     */
    public function getDeliveryZipCode()
    {
        return $this->deliveryZipCode;
    }

    /**
     * @param mixed $deliveryZipCode
     */
    public function setDeliveryZipCode($deliveryZipCode): void
    {
        $this->deliveryZipCode = $deliveryZipCode;
    }

    /**
     * @return mixed
     */
    public function getDeliveryCountry()
    {
        return $this->deliveryCountry;
    }

    /**
     * @param mixed $deliveryCountry
     */
    public function setDeliveryCountry($deliveryCountry): void
    {
        $this->deliveryCountry = $deliveryCountry;
    }

    /**
     * @return mixed
     */
    public function getDeliveryEmail()
    {
        return $this->deliveryEmail;
    }

    /**
     * @param mixed $deliveryEmail
     */
    public function setDeliveryEmail($deliveryEmail): void
    {
        $this->deliveryEmail = $deliveryEmail;
    }

    /**
     * @return mixed
     */
    public function getDeliveryPhone()
    {
        return $this->deliveryPhone;
    }

    /**
     * @param mixed $deliveryPhone
     */
    public function setDeliveryPhone($deliveryPhone): void
    {
        $this->deliveryPhone = $deliveryPhone;
    }

    /**
     * @return mixed
     */
    public function getAviaDepartureDate()
    {
        return $this->aviaDepartureDate;
    }

    /**
     * @param mixed $aviaDepartureDate
     */
    public function setAviaDepartureDate($aviaDepartureDate): void
    {
        $this->aviaDepartureDate = $aviaDepartureDate;
    }

    /**
     * @return mixed
     */
    public function getAviaLocationNumber()
    {
        return $this->aviaLocationNumber;
    }

    /**
     * @param mixed $aviaLocationNumber
     */
    public function setAviaLocationNumber($aviaLocationNumber): void
    {
        $this->aviaLocationNumber = $aviaLocationNumber;
    }

    /**
     * @return mixed
     */
    public function getAviaLocationCodes()
    {
        return $this->aviaLocationCodes;
    }

    /**
     * @param mixed $aviaLocationCodes
     */
    public function setAviaLocationCodes($aviaLocationCodes): void
    {
        $this->aviaLocationCodes = $aviaLocationCodes;
    }

    /**
     * @return mixed
     */
    public function getAviaFirstName()
    {
        return $this->aviaFirstName;
    }

    /**
     * @param mixed $aviaFirstName
     */
    public function setAviaFirstName($aviaFirstName): void
    {
        $this->aviaFirstName = $aviaFirstName;
    }

    /**
     * @return mixed
     */
    public function getAviaLastName()
    {
        return $this->aviaLastName;
    }

    /**
     * @param mixed $aviaLastName
     */
    public function setAviaLastName($aviaLastName): void
    {
        $this->aviaLastName = $aviaLastName;
    }

    /**
     * @return mixed
     */
    public function getAviaReservationCode()
    {
        return $this->aviaReservationCode;
    }

    /**
     * @param mixed $aviaReservationCode
     */
    public function setAviaReservationCode($aviaReservationCode): void
    {
        $this->aviaReservationCode = $aviaReservationCode;
    }

    /**
     * @return mixed
     */
    public function getRegularMode()
    {
        return $this->regularMode;
    }

    /**
     * @param mixed $regularMode
     */
    public function setRegularMode($regularMode): void
    {
        $this->regularMode = $regularMode;
    }

    /**
     * @return mixed
     */
    public function getRegularAmount()
    {
        return $this->regularAmount;
    }

    /**
     * @param mixed $regularAmount
     */
    public function setRegularAmount($regularAmount): void
    {
        $this->regularAmount = $regularAmount;
    }

    /**
     * @return mixed
     */
    public function getDateNext()
    {
        return $this->dateNext;
    }

    /**
     * @param mixed $dateNext
     */
    public function setDateNext($dateNext): void
    {
        $this->dateNext = $dateNext;
    }

    /**
     * @return mixed
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * @param mixed $dateEnd
     */
    public function setDateEnd($dateEnd): void
    {
        $this->dateEnd = $dateEnd;
    }

    /**
     * @return mixed
     */
    public function getRegularCount()
    {
        return $this->regularCount;
    }

    /**
     * @param mixed $regularCount
     */
    public function setRegularCount($regularCount): void
    {
        $this->regularCount = $regularCount;
    }

    /**
     * @return mixed
     */
    public function getRegularOn()
    {
        return $this->regularOn;
    }

    /**
     * @param mixed $regularOn
     */
    public function setRegularOn($regularOn): void
    {
        $this->regularOn = $regularOn;
    }

    /**
     * @return mixed
     */
    public function getPaymentSystems()
    {
        return $this->paymentSystems;
    }

    /**
     * @param mixed $paymentSystems
     */
    public function setPaymentSystems($paymentSystems): void
    {
        $this->paymentSystems = $paymentSystems;
    }

    /**
     * @return mixed
     */
    public function getDefaultPaymentSystem()
    {
        return $this->defaultPaymentSystem;
    }

    /**
     * @param mixed $defaultPaymentSystem
     */
    public function setDefaultPaymentSystem($defaultPaymentSystem): void
    {
        $this->defaultPaymentSystem = $defaultPaymentSystem;
    }

    public function signature(string $secretKey): string
    {
        return self::doSignature(
            [
                $this->getMerchantAccount(),
                $this->getMerchantDomainName(),
                $this->getOrderReference(),
                $this->getOrderDate(),
                $this->getAmount(),
                $this->getCurrency(),
                $this->getProductName(),
                $this->getProductCount(),
                $this->getProductPrice(),
            ],
            $secretKey
        );
    }

    public function predefine(CrassulaConfig $config, bool $force = false): void
    {
        if ($force || empty($this->getMerchantAccount())) {
            $this->setMerchantAccount($config->getMerchantAccount());
        }

        if ($force || empty($this->getMerchantDomainName())) {
            $this->setMerchantDomainName($config->getMerchantDomainName());
        }
    }

    public function controlRequire(bool $throws = true): bool
    {
        return $this->doRequire($this->__propsRequired, $throws);
    }
}
