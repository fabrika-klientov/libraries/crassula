<?php
/**
 *
 * @package   Crassula
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.22
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Entities;

use Crassula\Core\Helpers\Signatures;
use Crassula\Exceptions\CrassulaException;

abstract class Entity implements \JsonSerializable, \Stringable
{
    use Signatures;

    protected $__unresolved = [];
    protected $__propsRequired = [];

    /**
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
                continue;
            }

            $this->__unresolved[$key] = $value;
        }
    }

    /**
     * @return array
     */
    public function getUnresolved(): array
    {
        return $this->__unresolved;
    }

    /**
     * @return array
     */
    public function jsonData(): array
    {
        return array_filter(
            array_filter(
                get_object_vars($this),
                function ($key) {
                    return !str_starts_with($key, '__');
                },
                ARRAY_FILTER_USE_KEY
            ),
            function ($item) {
                return isset($item);
            }
        );
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return json_encode($this->jsonSerialize());
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * @param array $requiredList
     * @param bool $throws
     * @return bool
     * @throws CrassulaException
     */
    protected function doRequire(array $requiredList, bool $throws): bool
    {
        $acc = [];
        foreach ($requiredList as $item) {
            if (property_exists($this, $item) && empty($this->{$item})) {
                $acc[] = $item;
            }
        }

        if (empty($acc)) {
            return true;
        }

        if ($throws) {
            throw new CrassulaException(sprintf('Some fields [%s] are required.', join(', ', $acc)));
        }

        return false;
    }
}
