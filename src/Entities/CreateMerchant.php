<?php
/**
 *
 * @package   Crassula
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.07.28
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Entities;

use Crassula\Contracts\BeEntity;
use Crassula\Contracts\BePredefined;
use Crassula\Contracts\BeRequired;
use Crassula\Contracts\BeSignature;

/**
 * @deprecated
 * */
class CreateMerchant extends Entity implements BeEntity, BeSignature, BePredefined, BeRequired
{
    protected $merchantAccount;
    protected $merchantSignature;
    protected $site;
    protected $phone;
    protected $email;
    protected $description;
    protected $compensationCardNumber;
    protected $compensationCardExpYear;
    protected $compensationCardExpMonth;
    protected $compensationCardCvv;
    protected $compensationCardHolder;
    protected $compensationAccount;
    protected $compensationAccountMfo;
    protected $compensationAccountOkpo;
    protected $compensationAccountName;
    protected $compensationCardToken;

    protected $__propsRequired = [
        'merchantAccount',
        'site',
        'phone',
        'email',
    ];

    /**
     * @return mixed
     */
    public function getMerchantAccount()
    {
        return $this->merchantAccount;
    }

    /**
     * @param mixed $merchantAccount
     */
    public function setMerchantAccount($merchantAccount): void
    {
        $this->merchantAccount = $merchantAccount;
    }

    /**
     * @return mixed
     */
    public function getMerchantSignature()
    {
        return $this->merchantSignature;
    }

    /**
     * @param mixed $merchantSignature
     */
    public function setMerchantSignature($merchantSignature): void
    {
        $this->merchantSignature = $merchantSignature;
    }

    /**
     * @return mixed
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param mixed $site
     */
    public function setSite($site): void
    {
        $this->site = $site;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getCompensationCardNumber()
    {
        return $this->compensationCardNumber;
    }

    /**
     * @param mixed $compensationCardNumber
     */
    public function setCompensationCardNumber($compensationCardNumber): void
    {
        $this->compensationCardNumber = $compensationCardNumber;
    }

    /**
     * @return mixed
     */
    public function getCompensationCardExpYear()
    {
        return $this->compensationCardExpYear;
    }

    /**
     * @param mixed $compensationCardExpYear
     */
    public function setCompensationCardExpYear($compensationCardExpYear): void
    {
        $this->compensationCardExpYear = $compensationCardExpYear;
    }

    /**
     * @return mixed
     */
    public function getCompensationCardExpMonth()
    {
        return $this->compensationCardExpMonth;
    }

    /**
     * @param mixed $compensationCardExpMonth
     */
    public function setCompensationCardExpMonth($compensationCardExpMonth): void
    {
        $this->compensationCardExpMonth = $compensationCardExpMonth;
    }

    /**
     * @return mixed
     */
    public function getCompensationCardCvv()
    {
        return $this->compensationCardCvv;
    }

    /**
     * @param mixed $compensationCardCvv
     */
    public function setCompensationCardCvv($compensationCardCvv): void
    {
        $this->compensationCardCvv = $compensationCardCvv;
    }

    /**
     * @return mixed
     */
    public function getCompensationCardHolder()
    {
        return $this->compensationCardHolder;
    }

    /**
     * @param mixed $compensationCardHolder
     */
    public function setCompensationCardHolder($compensationCardHolder): void
    {
        $this->compensationCardHolder = $compensationCardHolder;
    }

    /**
     * @return mixed
     */
    public function getCompensationAccount()
    {
        return $this->compensationAccount;
    }

    /**
     * @param mixed $compensationAccount
     */
    public function setCompensationAccount($compensationAccount): void
    {
        $this->compensationAccount = $compensationAccount;
    }

    /**
     * @return mixed
     */
    public function getCompensationAccountMfo()
    {
        return $this->compensationAccountMfo;
    }

    /**
     * @param mixed $compensationAccountMfo
     */
    public function setCompensationAccountMfo($compensationAccountMfo): void
    {
        $this->compensationAccountMfo = $compensationAccountMfo;
    }

    /**
     * @return mixed
     */
    public function getCompensationAccountOkpo()
    {
        return $this->compensationAccountOkpo;
    }

    /**
     * @param mixed $compensationAccountOkpo
     */
    public function setCompensationAccountOkpo($compensationAccountOkpo): void
    {
        $this->compensationAccountOkpo = $compensationAccountOkpo;
    }

    /**
     * @return mixed
     */
    public function getCompensationAccountName()
    {
        return $this->compensationAccountName;
    }

    /**
     * @param mixed $compensationAccountName
     */
    public function setCompensationAccountName($compensationAccountName): void
    {
        $this->compensationAccountName = $compensationAccountName;
    }

    /**
     * @return mixed
     */
    public function getCompensationCardToken()
    {
        return $this->compensationCardToken;
    }

    /**
     * @param mixed $compensationCardToken
     */
    public function setCompensationCardToken($compensationCardToken): void
    {
        $this->compensationCardToken = $compensationCardToken;
    }

    public function signature(string $secretKey): string
    {
        return self::doSignature(
            [
                $this->getMerchantAccount(),
                $this->getSite(),
                $this->getPhone(),
                $this->getEmail(),
            ],
            $secretKey
        );
    }

    public function predefine(CrassulaConfig $config, bool $force = false): void
    {
        if ($force || empty($this->getMerchantAccount())) {
            $this->setMerchantAccount($config->getMerchantAccount());
        }
    }

    public function controlRequire(bool $throws = true): bool
    {
        return $this->doRequire($this->__propsRequired, $throws);
    }
}
