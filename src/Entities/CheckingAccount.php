<?php
/**
 *
 * @package   Crassula
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.23
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Entities;

use Crassula\Contracts\BeEntity;
use Crassula\Contracts\BePredefined;
use Crassula\Contracts\BeRequired;
use Crassula\Contracts\BeSignature;

class CheckingAccount extends Entity implements BeEntity, BeSignature, BePredefined, BeRequired
{
    protected $transactionType;
    protected $merchantAccount;
    protected $merchantAuthType;
    protected $orderReference;
    protected $amount;
    protected $currency;
    protected $iban;
    protected $okpo;
    protected $accountName;
    protected $merchantSignature;
    protected $apiVersion;
    protected $description;
    protected $serviceUrl;
    protected $recipientLastName;
    protected $recipientPhone;
    protected $recipientEmail;

    /**
     * @return mixed
     */
    public function getTransactionType()
    {
        return $this->transactionType;
    }

    /**
     * @param mixed $transactionType
     */
    public function setTransactionType($transactionType): void
    {
        $this->transactionType = $transactionType;
    }

    /**
     * @return mixed
     */
    public function getMerchantAccount()
    {
        return $this->merchantAccount;
    }

    /**
     * @param mixed $merchantAccount
     */
    public function setMerchantAccount($merchantAccount): void
    {
        $this->merchantAccount = $merchantAccount;
    }

    /**
     * @return mixed
     */
    public function getMerchantAuthType()
    {
        return $this->merchantAuthType;
    }

    /**
     * @param mixed $merchantAuthType
     */
    public function setMerchantAuthType($merchantAuthType): void
    {
        $this->merchantAuthType = $merchantAuthType;
    }

    /**
     * @return mixed
     */
    public function getOrderReference()
    {
        return $this->orderReference;
    }

    /**
     * @param mixed $orderReference
     */
    public function setOrderReference($orderReference): void
    {
        $this->orderReference = $orderReference;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * @param mixed $iban
     */
    public function setIban($iban): void
    {
        $this->iban = $iban;
    }

    /**
     * @return mixed
     */
    public function getOkpo()
    {
        return $this->okpo;
    }

    /**
     * @param mixed $okpo
     */
    public function setOkpo($okpo): void
    {
        $this->okpo = $okpo;
    }

    /**
     * @return mixed
     */
    public function getAccountName()
    {
        return $this->accountName;
    }

    /**
     * @param mixed $accountName
     */
    public function setAccountName($accountName): void
    {
        $this->accountName = $accountName;
    }

    /**
     * @return mixed
     */
    public function getMerchantSignature()
    {
        return $this->merchantSignature;
    }

    /**
     * @param mixed $merchantSignature
     */
    public function setMerchantSignature($merchantSignature): void
    {
        $this->merchantSignature = $merchantSignature;
    }

    /**
     * @return mixed
     */
    public function getApiVersion()
    {
        return $this->apiVersion;
    }

    /**
     * @param mixed $apiVersion
     */
    public function setApiVersion($apiVersion): void
    {
        $this->apiVersion = $apiVersion;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getServiceUrl()
    {
        return $this->serviceUrl;
    }

    /**
     * @param mixed $serviceUrl
     */
    public function setServiceUrl($serviceUrl): void
    {
        $this->serviceUrl = $serviceUrl;
    }

    /**
     * @return mixed
     */
    public function getRecipientLastName()
    {
        return $this->recipientLastName;
    }

    /**
     * @param mixed $recipientLastName
     */
    public function setRecipientLastName($recipientLastName): void
    {
        $this->recipientLastName = $recipientLastName;
    }

    /**
     * @return mixed
     */
    public function getRecipientPhone()
    {
        return $this->recipientPhone;
    }

    /**
     * @param mixed $recipientPhone
     */
    public function setRecipientPhone($recipientPhone): void
    {
        $this->recipientPhone = $recipientPhone;
    }

    /**
     * @return mixed
     */
    public function getRecipientEmail()
    {
        return $this->recipientEmail;
    }

    /**
     * @param mixed $recipientEmail
     */
    public function setRecipientEmail($recipientEmail): void
    {
        $this->recipientEmail = $recipientEmail;
    }

    public function signature(string $secretKey): string
    {
        return self::doSignature(
            [
                $this->getMerchantAccount(),
                $this->getOrderReference(),
                $this->getAmount(),
                $this->getCurrency(),
                $this->getIban(),
                $this->getOkpo(),
                $this->getAccountName(),
            ],
            $secretKey
        );
    }

    public function predefine(CrassulaConfig $config, bool $force = false): void
    {
        if ($force || empty($this->getMerchantAccount())) {
            $this->setMerchantAccount($config->getMerchantAccount());
        }

        if (empty($this->getTransactionType())) {
            $this->setTransactionType('P2P_ACCOUNT');
        }

        if (empty($this->getApiVersion())) {
            $this->setApiVersion(self::API_VERSION);
        }
    }

    public function controlRequire(bool $throws = true): bool
    {
        return $this->doRequire($this->__propsRequired, $throws);
    }
}
