<?php
/**
 *
 * @package   Crassula
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.23
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Entities;

use Crassula\Contracts\BeEntity;
use Crassula\Contracts\BePredefined;
use Crassula\Contracts\BeRequired;
use Crassula\Contracts\BeSignature;

class CreateInvoice extends Entity implements BeEntity, BeSignature, BePredefined, BeRequired
{
    protected $transactionType;
    protected $merchantAccount;
    protected $merchantTransactionType;
    protected $merchantAuthType;
    protected $merchantDomainName;
    protected $merchantSignature;
    protected $apiVersion;
    protected $language;
    protected $notifyMethod;
    protected $serviceUrl;
    protected $orderReference;
    protected $orderDate;
    protected $amount;
    protected $currency;
    protected $alternativeAmount;
    protected $alternativeCurrency;
    protected $holdTimeout;
    protected $orderTimeout;
    protected $productName;
    protected $productPrice;
    protected $productCount;
    protected $paymentSystems;
    protected $clientFirstName;
    protected $clientLastName;
    protected $clientEmail;
    protected $clientPhone;

    protected $__propsRequired = [
        'merchantDomainName',
        'orderReference',
        'orderDate',
        'amount',
        'currency',
        'products',
    ];

    /**
     * @return mixed
     */
    public function getTransactionType()
    {
        return $this->transactionType;
    }

    /**
     * @param mixed $transactionType
     */
    public function setTransactionType($transactionType): void
    {
        $this->transactionType = $transactionType;
    }

    /**
     * @return mixed
     */
    public function getMerchantAccount()
    {
        return $this->merchantAccount;
    }

    /**
     * @param mixed $merchantAccount
     */
    public function setMerchantAccount($merchantAccount): void
    {
        $this->merchantAccount = $merchantAccount;
    }

    /**
     * @return mixed
     */
    public function getMerchantTransactionType()
    {
        return $this->merchantTransactionType;
    }

    /**
     * @param mixed $merchantTransactionType
     */
    public function setMerchantTransactionType($merchantTransactionType): void
    {
        $this->merchantTransactionType = $merchantTransactionType;
    }

    /**
     * @return mixed
     */
    public function getMerchantAuthType()
    {
        return $this->merchantAuthType;
    }

    /**
     * @param mixed $merchantAuthType
     */
    public function setMerchantAuthType($merchantAuthType): void
    {
        $this->merchantAuthType = $merchantAuthType;
    }

    /**
     * @return mixed
     */
    public function getMerchantDomainName()
    {
        return $this->merchantDomainName;
    }

    /**
     * @param mixed $merchantDomainName
     */
    public function setMerchantDomainName($merchantDomainName): void
    {
        $this->merchantDomainName = $merchantDomainName;
    }

    /**
     * @return mixed
     */
    public function getMerchantSignature()
    {
        return $this->merchantSignature;
    }

    /**
     * @param mixed $merchantSignature
     */
    public function setMerchantSignature($merchantSignature): void
    {
        $this->merchantSignature = $merchantSignature;
    }

    /**
     * @return mixed
     */
    public function getApiVersion()
    {
        return $this->apiVersion;
    }

    /**
     * @param mixed $apiVersion
     */
    public function setApiVersion($apiVersion): void
    {
        $this->apiVersion = $apiVersion;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language): void
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getNotifyMethod()
    {
        return $this->notifyMethod;
    }

    /**
     * @param mixed $notifyMethod
     */
    public function setNotifyMethod($notifyMethod): void
    {
        $this->notifyMethod = $notifyMethod;
    }

    /**
     * @return mixed
     */
    public function getServiceUrl()
    {
        return $this->serviceUrl;
    }

    /**
     * @param mixed $serviceUrl
     */
    public function setServiceUrl($serviceUrl): void
    {
        $this->serviceUrl = $serviceUrl;
    }

    /**
     * @return mixed
     */
    public function getOrderReference()
    {
        return $this->orderReference;
    }

    /**
     * @param mixed $orderReference
     */
    public function setOrderReference($orderReference): void
    {
        $this->orderReference = $orderReference;
    }

    /**
     * @return mixed
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * @param mixed $orderDate
     */
    public function setOrderDate($orderDate): void
    {
        $this->orderDate = $orderDate;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getAlternativeAmount()
    {
        return $this->alternativeAmount;
    }

    /**
     * @param mixed $alternativeAmount
     */
    public function setAlternativeAmount($alternativeAmount): void
    {
        $this->alternativeAmount = $alternativeAmount;
    }

    /**
     * @return mixed
     */
    public function getAlternativeCurrency()
    {
        return $this->alternativeCurrency;
    }

    /**
     * @param mixed $alternativeCurrency
     */
    public function setAlternativeCurrency($alternativeCurrency): void
    {
        $this->alternativeCurrency = $alternativeCurrency;
    }

    /**
     * @return mixed
     */
    public function getHoldTimeout()
    {
        return $this->holdTimeout;
    }

    /**
     * @param mixed $holdTimeout
     */
    public function setHoldTimeout($holdTimeout): void
    {
        $this->holdTimeout = $holdTimeout;
    }

    /**
     * @return mixed
     */
    public function getOrderTimeout()
    {
        return $this->orderTimeout;
    }

    /**
     * @param mixed $orderTimeout
     */
    public function setOrderTimeout($orderTimeout): void
    {
        $this->orderTimeout = $orderTimeout;
    }

    /**
     * @return mixed
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * @param mixed $productName
     */
    public function setProductName($productName): void
    {
        $this->productName = $productName;
    }

    /**
     * @return mixed
     */
    public function getProductPrice()
    {
        return $this->productPrice;
    }

    /**
     * @param mixed $productPrice
     */
    public function setProductPrice($productPrice): void
    {
        $this->productPrice = $productPrice;
    }

    /**
     * @return mixed
     */
    public function getProductCount()
    {
        return $this->productCount;
    }

    /**
     * @param mixed $productCount
     */
    public function setProductCount($productCount): void
    {
        $this->productCount = $productCount;
    }

    /**
     * @return mixed
     */
    public function getPaymentSystems()
    {
        return $this->paymentSystems;
    }

    /**
     * @param mixed $paymentSystems
     */
    public function setPaymentSystems($paymentSystems): void
    {
        $this->paymentSystems = $paymentSystems;
    }

    /**
     * @return mixed
     */
    public function getClientFirstName()
    {
        return $this->clientFirstName;
    }

    /**
     * @param mixed $clientFirstName
     */
    public function setClientFirstName($clientFirstName): void
    {
        $this->clientFirstName = $clientFirstName;
    }

    /**
     * @return mixed
     */
    public function getClientLastName()
    {
        return $this->clientLastName;
    }

    /**
     * @param mixed $clientLastName
     */
    public function setClientLastName($clientLastName): void
    {
        $this->clientLastName = $clientLastName;
    }

    /**
     * @return mixed
     */
    public function getClientEmail()
    {
        return $this->clientEmail;
    }

    /**
     * @param mixed $clientEmail
     */
    public function setClientEmail($clientEmail): void
    {
        $this->clientEmail = $clientEmail;
    }

    /**
     * @return mixed
     */
    public function getClientPhone()
    {
        return $this->clientPhone;
    }

    /**
     * @param mixed $clientPhone
     */
    public function setClientPhone($clientPhone): void
    {
        $this->clientPhone = $clientPhone;
    }

    public function signature(string $secretKey): string
    {
        return self::doSignature(
            [
                $this->getMerchantAccount(),
                $this->getMerchantDomainName(),
                $this->getOrderReference(),
                $this->getOrderDate(),
                $this->getAmount(),
                $this->getCurrency(),
                $this->getProductName(),
                $this->getProductCount(),
                $this->getProductPrice(),
            ],
            $secretKey
        );
    }

    public function predefine(CrassulaConfig $config, bool $force = false): void
    {
        if ($force || empty($this->getMerchantAccount())) {
            $this->setMerchantAccount($config->getMerchantAccount());
        }

        if ($force || empty($this->getMerchantDomainName())) {
            $this->setMerchantDomainName($config->getMerchantDomainName());
        }

        if (empty($this->getTransactionType())) {
            $this->setTransactionType('CREATE_INVOICE');
        }

        if (empty($this->getApiVersion())) {
            $this->setApiVersion(self::API_VERSION);
        }
    }

    public function controlRequire(bool $throws = true): bool
    {
        return $this->doRequire($this->__propsRequired, $throws);
    }
}
