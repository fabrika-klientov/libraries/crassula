<?php
/**
 *
 * @package   Crassula
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.23
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Entities;

use Crassula\Contracts\BeEntity;

class CreateInvoiceResult extends Entity implements BeEntity
{
    protected $reason;
    protected $reasonCode;
    protected $invoiceUrl;
    protected $qrCode;

    /**
     * @return mixed
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param mixed $reason
     */
    public function setReason($reason): void
    {
        $this->reason = $reason;
    }

    /**
     * @return mixed
     */
    public function getReasonCode()
    {
        return $this->reasonCode;
    }

    /**
     * @param mixed $reasonCode
     */
    public function setReasonCode($reasonCode): void
    {
        $this->reasonCode = $reasonCode;
    }

    /**
     * @return mixed
     */
    public function getInvoiceUrl()
    {
        return $this->invoiceUrl;
    }

    /**
     * @param mixed $invoiceUrl
     */
    public function setInvoiceUrl($invoiceUrl): void
    {
        $this->invoiceUrl = $invoiceUrl;
    }

    /**
     * @return mixed
     */
    public function getQrCode()
    {
        return $this->qrCode;
    }

    /**
     * @param mixed $qrCode
     */
    public function setQrCode($qrCode): void
    {
        $this->qrCode = $qrCode;
    }
}
