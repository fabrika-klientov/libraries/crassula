<?php
/**
 *
 * @package   Crassula
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.23
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Entities;

use Crassula\Contracts\BeEntity;
use Crassula\Contracts\BePredefined;
use Crassula\Contracts\BeRequired;
use Crassula\Contracts\BeSignature;

class Currency extends Entity implements BeEntity, BeSignature, BePredefined, BeRequired
{
    protected $transactionType;
    protected $merchantAccount;
    protected $merchantSignature;
    protected $apiVersion;
    protected $orderDate;
    protected $currency;

    /**
     * @return mixed
     */
    public function getTransactionType()
    {
        return $this->transactionType;
    }

    /**
     * @param mixed $transactionType
     */
    public function setTransactionType($transactionType): void
    {
        $this->transactionType = $transactionType;
    }

    /**
     * @return mixed
     */
    public function getMerchantAccount()
    {
        return $this->merchantAccount;
    }

    /**
     * @param mixed $merchantAccount
     */
    public function setMerchantAccount($merchantAccount): void
    {
        $this->merchantAccount = $merchantAccount;
    }

    /**
     * @return mixed
     */
    public function getMerchantSignature()
    {
        return $this->merchantSignature;
    }

    /**
     * @param mixed $merchantSignature
     */
    public function setMerchantSignature($merchantSignature): void
    {
        $this->merchantSignature = $merchantSignature;
    }

    /**
     * @return mixed
     */
    public function getApiVersion()
    {
        return $this->apiVersion;
    }

    /**
     * @param mixed $apiVersion
     */
    public function setApiVersion($apiVersion): void
    {
        $this->apiVersion = $apiVersion;
    }

    /**
     * @return mixed
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * @param mixed $orderDate
     */
    public function setOrderDate($orderDate): void
    {
        $this->orderDate = $orderDate;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency): void
    {
        $this->currency = $currency;
    }

    public function signature(string $secretKey): string
    {
        return self::doSignature(
            [
                $this->getMerchantAccount(),
                $this->getOrderDate(),
            ],
            $secretKey
        );
    }

    public function predefine(CrassulaConfig $config, bool $force = false): void
    {
        if ($force || empty($this->getMerchantAccount())) {
            $this->setMerchantAccount($config->getMerchantAccount());
        }

        if (empty($this->getTransactionType())) {
            $this->setTransactionType('CURRENCY_RATES');
        }

        if (empty($this->getApiVersion())) {
            $this->setApiVersion(self::API_VERSION);
        }
    }

    public function controlRequire(bool $throws = true): bool
    {
        return $this->doRequire($this->__propsRequired, $throws);
    }
}
