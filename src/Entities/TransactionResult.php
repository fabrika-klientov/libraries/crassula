<?php
/**
 *
 * @package   Crassula
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.23
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Entities;

use Crassula\Contracts\BeEntity;
use Illuminate\Support\Collection;

class TransactionResult extends Entity implements BeEntity
{
    protected $reason;
    protected $reasonCode;
    protected $transactionList;

    public function __construct(array $data = [])
    {
        parent::__construct($data);

        $this->transactionList = new Collection(
            empty($data['transactionList']) ? [] : array_map(
                function ($item) {
                    return new Transaction($item);
                },
                $data['transactionList']
            )
        );
    }

    /**
     * @return mixed
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param mixed $reason
     */
    public function setReason($reason): void
    {
        $this->reason = $reason;
    }

    /**
     * @return mixed
     */
    public function getReasonCode()
    {
        return $this->reasonCode;
    }

    /**
     * @param mixed $reasonCode
     */
    public function setReasonCode($reasonCode): void
    {
        $this->reasonCode = $reasonCode;
    }

    /**
     * @return Collection
     */
    public function getTransactionList()
    {
        return $this->transactionList;
    }

    /**
     * @param Collection $transactionList
     */
    public function setTransactionList($transactionList): void
    {
        $this->transactionList = $transactionList;
    }
}
