<?php
/**
 *
 * @package   Crassula
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.23
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Entities;

use Crassula\Contracts\BeEntity;

class CreditCardResult extends Entity implements BeEntity
{
    protected $merchantAccount;
    protected $orderReference;
    protected $merchantSignature;
    protected $amount;
    protected $currency;
    protected $authCode;
    protected $cardPan;
    protected $cardType;
    protected $issuerBankName;
    protected $createdDate;
    protected $processingDate;
    protected $transactionStatus;
    protected $reason;
    protected $reasonCode;

    /**
     * @return mixed
     */
    public function getMerchantAccount()
    {
        return $this->merchantAccount;
    }

    /**
     * @param mixed $merchantAccount
     */
    public function setMerchantAccount($merchantAccount): void
    {
        $this->merchantAccount = $merchantAccount;
    }

    /**
     * @return mixed
     */
    public function getOrderReference()
    {
        return $this->orderReference;
    }

    /**
     * @param mixed $orderReference
     */
    public function setOrderReference($orderReference): void
    {
        $this->orderReference = $orderReference;
    }

    /**
     * @return mixed
     */
    public function getMerchantSignature()
    {
        return $this->merchantSignature;
    }

    /**
     * @param mixed $merchantSignature
     */
    public function setMerchantSignature($merchantSignature): void
    {
        $this->merchantSignature = $merchantSignature;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getAuthCode()
    {
        return $this->authCode;
    }

    /**
     * @param mixed $authCode
     */
    public function setAuthCode($authCode): void
    {
        $this->authCode = $authCode;
    }

    /**
     * @return mixed
     */
    public function getCardPan()
    {
        return $this->cardPan;
    }

    /**
     * @param mixed $cardPan
     */
    public function setCardPan($cardPan): void
    {
        $this->cardPan = $cardPan;
    }

    /**
     * @return mixed
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    /**
     * @param mixed $cardType
     */
    public function setCardType($cardType): void
    {
        $this->cardType = $cardType;
    }

    /**
     * @return mixed
     */
    public function getIssuerBankName()
    {
        return $this->issuerBankName;
    }

    /**
     * @param mixed $issuerBankName
     */
    public function setIssuerBankName($issuerBankName): void
    {
        $this->issuerBankName = $issuerBankName;
    }

    /**
     * @return mixed
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param mixed $createdDate
     */
    public function setCreatedDate($createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return mixed
     */
    public function getProcessingDate()
    {
        return $this->processingDate;
    }

    /**
     * @param mixed $processingDate
     */
    public function setProcessingDate($processingDate): void
    {
        $this->processingDate = $processingDate;
    }

    /**
     * @return mixed
     */
    public function getTransactionStatus()
    {
        return $this->transactionStatus;
    }

    /**
     * @param mixed $transactionStatus
     */
    public function setTransactionStatus($transactionStatus): void
    {
        $this->transactionStatus = $transactionStatus;
    }

    /**
     * @return mixed
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param mixed $reason
     */
    public function setReason($reason): void
    {
        $this->reason = $reason;
    }

    /**
     * @return mixed
     */
    public function getReasonCode()
    {
        return $this->reasonCode;
    }

    /**
     * @param mixed $reasonCode
     */
    public function setReasonCode($reasonCode): void
    {
        $this->reasonCode = $reasonCode;
    }
}
