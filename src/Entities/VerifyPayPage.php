<?php
/**
 *
 * @package   Crassula
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.23
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Entities;

use Crassula\Contracts\BeEntity;
use Crassula\Contracts\BePredefined;
use Crassula\Contracts\BeRequired;
use Crassula\Contracts\BeSignature;

class VerifyPayPage extends Entity implements BeEntity, BeSignature, BePredefined, BeRequired
{
    protected $merchantAccount;
    protected $merchantAuthType;
    protected $merchantDomainName;
    protected $merchantSignature;
    protected $apiVersion;
    protected $returnUrl;
    protected $serviceUrl;
    protected $orderReference;
    protected $amount;
    protected $currency;
    protected $paymentSystem;
    protected $clientEmail;
    protected $clientPhone;
    protected $clientCountry;
    protected $clientAddress;
    protected $clientCity;
    protected $clientState;
    protected $clientZipCode;

    /**
     * @return mixed
     */
    public function getMerchantAccount()
    {
        return $this->merchantAccount;
    }

    /**
     * @param mixed $merchantAccount
     */
    public function setMerchantAccount($merchantAccount): void
    {
        $this->merchantAccount = $merchantAccount;
    }

    /**
     * @return mixed
     */
    public function getMerchantAuthType()
    {
        return $this->merchantAuthType;
    }

    /**
     * @param mixed $merchantAuthType
     */
    public function setMerchantAuthType($merchantAuthType): void
    {
        $this->merchantAuthType = $merchantAuthType;
    }

    /**
     * @return mixed
     */
    public function getMerchantDomainName()
    {
        return $this->merchantDomainName;
    }

    /**
     * @param mixed $merchantDomainName
     */
    public function setMerchantDomainName($merchantDomainName): void
    {
        $this->merchantDomainName = $merchantDomainName;
    }

    /**
     * @return mixed
     */
    public function getMerchantSignature()
    {
        return $this->merchantSignature;
    }

    /**
     * @param mixed $merchantSignature
     */
    public function setMerchantSignature($merchantSignature): void
    {
        $this->merchantSignature = $merchantSignature;
    }

    /**
     * @return mixed
     */
    public function getApiVersion()
    {
        return $this->apiVersion;
    }

    /**
     * @param mixed $apiVersion
     */
    public function setApiVersion($apiVersion): void
    {
        $this->apiVersion = $apiVersion;
    }

    /**
     * @return mixed
     */
    public function getReturnUrl()
    {
        return $this->returnUrl;
    }

    /**
     * @param mixed $returnUrl
     */
    public function setReturnUrl($returnUrl): void
    {
        $this->returnUrl = $returnUrl;
    }

    /**
     * @return mixed
     */
    public function getServiceUrl()
    {
        return $this->serviceUrl;
    }

    /**
     * @param mixed $serviceUrl
     */
    public function setServiceUrl($serviceUrl): void
    {
        $this->serviceUrl = $serviceUrl;
    }

    /**
     * @return mixed
     */
    public function getOrderReference()
    {
        return $this->orderReference;
    }

    /**
     * @param mixed $orderReference
     */
    public function setOrderReference($orderReference): void
    {
        $this->orderReference = $orderReference;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getPaymentSystem()
    {
        return $this->paymentSystem;
    }

    /**
     * @param mixed $paymentSystem
     */
    public function setPaymentSystem($paymentSystem): void
    {
        $this->paymentSystem = $paymentSystem;
    }

    /**
     * @return mixed
     */
    public function getClientEmail()
    {
        return $this->clientEmail;
    }

    /**
     * @param mixed $clientEmail
     */
    public function setClientEmail($clientEmail): void
    {
        $this->clientEmail = $clientEmail;
    }

    /**
     * @return mixed
     */
    public function getClientPhone()
    {
        return $this->clientPhone;
    }

    /**
     * @param mixed $clientPhone
     */
    public function setClientPhone($clientPhone): void
    {
        $this->clientPhone = $clientPhone;
    }

    /**
     * @return mixed
     */
    public function getClientCountry()
    {
        return $this->clientCountry;
    }

    /**
     * @param mixed $clientCountry
     */
    public function setClientCountry($clientCountry): void
    {
        $this->clientCountry = $clientCountry;
    }

    /**
     * @return mixed
     */
    public function getClientAddress()
    {
        return $this->clientAddress;
    }

    /**
     * @param mixed $clientAddress
     */
    public function setClientAddress($clientAddress): void
    {
        $this->clientAddress = $clientAddress;
    }

    /**
     * @return mixed
     */
    public function getClientCity()
    {
        return $this->clientCity;
    }

    /**
     * @param mixed $clientCity
     */
    public function setClientCity($clientCity): void
    {
        $this->clientCity = $clientCity;
    }

    /**
     * @return mixed
     */
    public function getClientState()
    {
        return $this->clientState;
    }

    /**
     * @param mixed $clientState
     */
    public function setClientState($clientState): void
    {
        $this->clientState = $clientState;
    }

    /**
     * @return mixed
     */
    public function getClientZipCode()
    {
        return $this->clientZipCode;
    }

    /**
     * @param mixed $clientZipCode
     */
    public function setClientZipCode($clientZipCode): void
    {
        $this->clientZipCode = $clientZipCode;
    }

    public function signature(string $secretKey): string
    {
        return self::doSignature(
            [
                $this->getMerchantAccount(),
                $this->getMerchantDomainName(),
                $this->getOrderReference(),
                $this->getAmount(),
                $this->getCurrency(),
            ],
            $secretKey
        );
    }

    public function predefine(CrassulaConfig $config, bool $force = false): void
    {
        if ($force || empty($this->getMerchantAccount())) {
            $this->setMerchantAccount($config->getMerchantAccount());
        }

        if ($force || empty($this->getMerchantDomainName())) {
            $this->setMerchantDomainName($config->getMerchantDomainName());
        }

        if (empty($this->getApiVersion())) {
            $this->setApiVersion(self::API_VERSION);
        }
    }

    public function controlRequire(bool $throws = true): bool
    {
        return $this->doRequire($this->__propsRequired, $throws);
    }
}
