<?php
/**
 *
 * @package   Crassula
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.23
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Entities;

use Crassula\Contracts\BeEntity;
use Crassula\Contracts\BePredefined;
use Crassula\Contracts\BeRequired;
use Crassula\Contracts\BeSignature;

class Charge extends Entity implements BeEntity, BeSignature, BePredefined, BeRequired
{
    protected $transactionType;
    protected $merchantAccount;
    protected $merchantAuthType;
    protected $merchantDomainName;
    protected $merchantTransactionType;
    protected $merchantTransactionSecureType;
    protected $merchantSignature;
    protected $apiVersion;
    protected $serviceUrl;
    protected $orderReference;
    protected $orderDate;
    protected $amount;
    protected $currency;
    protected $holdTimeout;
    protected $card;
    protected $expMonth;
    protected $expYear;
    protected $cardCvv;
    protected $cardHolder;
    protected $recToken;
    protected $productName;
    protected $productPrice;
    protected $productCount;
    protected $clientAccountId;
    protected $socialUri;
    protected $clientFirstName;
    protected $clientLastName;
    protected $clientEmail;
    protected $clientPhone;
    protected $clientCountry;
    protected $clientIpAddress;
    protected $clientAddress;
    protected $clientCity;
    protected $clientState;
    protected $clientZipCode;

    protected $__propsRequired = [
        'orderReference',
        'amount',
        'currency',
        'products',
        'orderDate',
        'merchantDomainName',
    ];

    /**
     * @return mixed
     */
    public function getTransactionType()
    {
        return $this->transactionType;
    }

    /**
     * @param mixed $transactionType
     */
    public function setTransactionType($transactionType): void
    {
        $this->transactionType = $transactionType;
    }

    /**
     * @return mixed
     */
    public function getMerchantAccount()
    {
        return $this->merchantAccount;
    }

    /**
     * @param mixed $merchantAccount
     */
    public function setMerchantAccount($merchantAccount): void
    {
        $this->merchantAccount = $merchantAccount;
    }

    /**
     * @return mixed
     */
    public function getMerchantAuthType()
    {
        return $this->merchantAuthType;
    }

    /**
     * @param mixed $merchantAuthType
     */
    public function setMerchantAuthType($merchantAuthType): void
    {
        $this->merchantAuthType = $merchantAuthType;
    }

    /**
     * @return mixed
     */
    public function getMerchantDomainName()
    {
        return $this->merchantDomainName;
    }

    /**
     * @param mixed $merchantDomainName
     */
    public function setMerchantDomainName($merchantDomainName): void
    {
        $this->merchantDomainName = $merchantDomainName;
    }

    /**
     * @return mixed
     */
    public function getMerchantTransactionType()
    {
        return $this->merchantTransactionType;
    }

    /**
     * @param mixed $merchantTransactionType
     */
    public function setMerchantTransactionType($merchantTransactionType): void
    {
        $this->merchantTransactionType = $merchantTransactionType;
    }

    /**
     * @return mixed
     */
    public function getMerchantTransactionSecureType()
    {
        return $this->merchantTransactionSecureType;
    }

    /**
     * @param mixed $merchantTransactionSecureType
     */
    public function setMerchantTransactionSecureType($merchantTransactionSecureType): void
    {
        $this->merchantTransactionSecureType = $merchantTransactionSecureType;
    }

    /**
     * @return mixed
     */
    public function getMerchantSignature()
    {
        return $this->merchantSignature;
    }

    /**
     * @param mixed $merchantSignature
     */
    public function setMerchantSignature($merchantSignature): void
    {
        $this->merchantSignature = $merchantSignature;
    }

    /**
     * @return mixed
     */
    public function getApiVersion()
    {
        return $this->apiVersion;
    }

    /**
     * @param mixed $apiVersion
     */
    public function setApiVersion($apiVersion): void
    {
        $this->apiVersion = $apiVersion;
    }

    /**
     * @return mixed
     */
    public function getServiceUrl()
    {
        return $this->serviceUrl;
    }

    /**
     * @param mixed $serviceUrl
     */
    public function setServiceUrl($serviceUrl): void
    {
        $this->serviceUrl = $serviceUrl;
    }

    /**
     * @return mixed
     */
    public function getOrderReference()
    {
        return $this->orderReference;
    }

    /**
     * @param mixed $orderReference
     */
    public function setOrderReference($orderReference): void
    {
        $this->orderReference = $orderReference;
    }

    /**
     * @return mixed
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * @param mixed $orderDate
     */
    public function setOrderDate($orderDate): void
    {
        $this->orderDate = $orderDate;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getHoldTimeout()
    {
        return $this->holdTimeout;
    }

    /**
     * @param mixed $holdTimeout
     */
    public function setHoldTimeout($holdTimeout): void
    {
        $this->holdTimeout = $holdTimeout;
    }

    /**
     * @return mixed
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * @param mixed $card
     */
    public function setCard($card): void
    {
        $this->card = $card;
    }

    /**
     * @return mixed
     */
    public function getExpMonth()
    {
        return $this->expMonth;
    }

    /**
     * @param mixed $expMonth
     */
    public function setExpMonth($expMonth): void
    {
        $this->expMonth = $expMonth;
    }

    /**
     * @return mixed
     */
    public function getExpYear()
    {
        return $this->expYear;
    }

    /**
     * @param mixed $expYear
     */
    public function setExpYear($expYear): void
    {
        $this->expYear = $expYear;
    }

    /**
     * @return mixed
     */
    public function getCardCvv()
    {
        return $this->cardCvv;
    }

    /**
     * @param mixed $cardCvv
     */
    public function setCardCvv($cardCvv): void
    {
        $this->cardCvv = $cardCvv;
    }

    /**
     * @return mixed
     */
    public function getCardHolder()
    {
        return $this->cardHolder;
    }

    /**
     * @param mixed $cardHolder
     */
    public function setCardHolder($cardHolder): void
    {
        $this->cardHolder = $cardHolder;
    }

    /**
     * @return mixed
     */
    public function getRecToken()
    {
        return $this->recToken;
    }

    /**
     * @param mixed $recToken
     */
    public function setRecToken($recToken): void
    {
        $this->recToken = $recToken;
    }

    /**
     * @return mixed
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * @param mixed $productName
     */
    public function setProductName($productName): void
    {
        $this->productName = $productName;
    }

    /**
     * @return mixed
     */
    public function getProductPrice()
    {
        return $this->productPrice;
    }

    /**
     * @param mixed $productPrice
     */
    public function setProductPrice($productPrice): void
    {
        $this->productPrice = $productPrice;
    }

    /**
     * @return mixed
     */
    public function getProductCount()
    {
        return $this->productCount;
    }

    /**
     * @param mixed $productCount
     */
    public function setProductCount($productCount): void
    {
        $this->productCount = $productCount;
    }

    /**
     * @return mixed
     */
    public function getClientAccountId()
    {
        return $this->clientAccountId;
    }

    /**
     * @param mixed $clientAccountId
     */
    public function setClientAccountId($clientAccountId): void
    {
        $this->clientAccountId = $clientAccountId;
    }

    /**
     * @return mixed
     */
    public function getSocialUri()
    {
        return $this->socialUri;
    }

    /**
     * @param mixed $socialUri
     */
    public function setSocialUri($socialUri): void
    {
        $this->socialUri = $socialUri;
    }

    /**
     * @return mixed
     */
    public function getClientFirstName()
    {
        return $this->clientFirstName;
    }

    /**
     * @param mixed $clientFirstName
     */
    public function setClientFirstName($clientFirstName): void
    {
        $this->clientFirstName = $clientFirstName;
    }

    /**
     * @return mixed
     */
    public function getClientLastName()
    {
        return $this->clientLastName;
    }

    /**
     * @param mixed $clientLastName
     */
    public function setClientLastName($clientLastName): void
    {
        $this->clientLastName = $clientLastName;
    }

    /**
     * @return mixed
     */
    public function getClientEmail()
    {
        return $this->clientEmail;
    }

    /**
     * @param mixed $clientEmail
     */
    public function setClientEmail($clientEmail): void
    {
        $this->clientEmail = $clientEmail;
    }

    /**
     * @return mixed
     */
    public function getClientPhone()
    {
        return $this->clientPhone;
    }

    /**
     * @param mixed $clientPhone
     */
    public function setClientPhone($clientPhone): void
    {
        $this->clientPhone = $clientPhone;
    }

    /**
     * @return mixed
     */
    public function getClientCountry()
    {
        return $this->clientCountry;
    }

    /**
     * @param mixed $clientCountry
     */
    public function setClientCountry($clientCountry): void
    {
        $this->clientCountry = $clientCountry;
    }

    /**
     * @return mixed
     */
    public function getClientIpAddress()
    {
        return $this->clientIpAddress;
    }

    /**
     * @param mixed $clientIpAddress
     */
    public function setClientIpAddress($clientIpAddress): void
    {
        $this->clientIpAddress = $clientIpAddress;
    }

    /**
     * @return mixed
     */
    public function getClientAddress()
    {
        return $this->clientAddress;
    }

    /**
     * @param mixed $clientAddress
     */
    public function setClientAddress($clientAddress): void
    {
        $this->clientAddress = $clientAddress;
    }

    /**
     * @return mixed
     */
    public function getClientCity()
    {
        return $this->clientCity;
    }

    /**
     * @param mixed $clientCity
     */
    public function setClientCity($clientCity): void
    {
        $this->clientCity = $clientCity;
    }

    /**
     * @return mixed
     */
    public function getClientState()
    {
        return $this->clientState;
    }

    /**
     * @param mixed $clientState
     */
    public function setClientState($clientState): void
    {
        $this->clientState = $clientState;
    }

    /**
     * @return mixed
     */
    public function getClientZipCode()
    {
        return $this->clientZipCode;
    }

    /**
     * @param mixed $clientZipCode
     */
    public function setClientZipCode($clientZipCode): void
    {
        $this->clientZipCode = $clientZipCode;
    }

    public function signature(string $secretKey): string
    {
        return self::doSignature(
            [
                $this->getMerchantAccount(),
                $this->getMerchantDomainName(),
                $this->getOrderReference(),
                $this->getOrderDate(),
                $this->getAmount(),
                $this->getCurrency(),
                $this->getProductName(),
                $this->getProductCount(),
                $this->getProductPrice(),
            ],
            $secretKey
        );
    }

    public function predefine(CrassulaConfig $config, bool $force = false): void
    {
        if ($force || empty($this->getMerchantAccount())) {
            $this->setMerchantAccount($config->getMerchantAccount());
        }

        if ($force || empty($this->getMerchantDomainName())) {
            $this->setMerchantDomainName($config->getMerchantDomainName());
        }

        if (empty($this->getTransactionType())) {
            $this->setTransactionType('CHARGE');
        }

        if (empty($this->getApiVersion())) {
            $this->setApiVersion(self::API_VERSION); // available 2
        }
    }

    public function controlRequire(bool $throws = true): bool
    {
        return $this->doRequire($this->__propsRequired, $throws);
    }
}
