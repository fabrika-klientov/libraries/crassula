<?php
/**
 *
 * @package   Crassula
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.23
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Exceptions;

use Exception;
use Throwable;

class CrassulaException extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->setMessage($message);
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message): void
    {
        $this->message = 'CRASSULA:: ' . $message;
    }
}
