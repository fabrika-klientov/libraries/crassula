<?php
/**
 *
 * @package   Crassula
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.22
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Contracts;

interface BeEntity
{
    public const API_VERSION = 1;
}
