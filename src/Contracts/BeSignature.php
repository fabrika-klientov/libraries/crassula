<?php
/**
 *
 * @package   Crassula
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.23
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Contracts;

interface BeSignature
{
    /**
     * @param string $secretKey
     * @return string
     * @throws \Crassula\Exceptions\CrassulaException
     */
    public function signature(string $secretKey): string;
}
