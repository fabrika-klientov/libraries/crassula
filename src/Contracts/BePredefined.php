<?php
/**
 *
 * @package   Crassula
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.24
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Contracts;

use Crassula\Entities\CrassulaConfig;

interface BePredefined
{
    /**
     * @param CrassulaConfig $config
     * @param bool $force
     * @return void
     */
    public function predefine(CrassulaConfig $config, bool $force = false): void;
}
