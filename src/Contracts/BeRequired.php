<?php
/**
 *
 * @package   Crassula
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.24
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula\Contracts;

interface BeRequired
{
    /**
     * @param bool $throws
     * @return bool
     * @throws \Crassula\Exceptions\CrassulaException
     */
    public function controlRequire(bool $throws = true): bool;
}
