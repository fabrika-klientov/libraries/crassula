<?php
/**
 *
 * @package   Crassula
 * @category  Crassula
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.22
 * @link      https://fabrika-klientov.ua
 */

namespace Crassula;

use Crassula\Core\Helpers\Signatures;
use Crassula\Core\Http\HttpClient;
use Crassula\Core\Services\CheckingAccountService;
use Crassula\Core\Services\CreditService;
use Crassula\Core\Services\CurrencyService;
use Crassula\Core\Services\InvoiceService;
use Crassula\Core\Services\MerchantService;
use Crassula\Core\Services\PaymentService;
use Crassula\Core\Services\TransactionService;
use Crassula\Core\Services\TransferService;
use Crassula\Core\Services\VerifyService;
use Crassula\Entities\Charge;
use Crassula\Entities\CheckingAccount;
use Crassula\Entities\CheckStatus;
use Crassula\Entities\CrassulaConfig;
use Crassula\Entities\CreateInvoice;
use Crassula\Entities\CreditCard;
use Crassula\Entities\Purchase;
use Crassula\Entities\Refund;
use Crassula\Entities\RemoveInvoice;
use Crassula\Entities\Settle;
use Crassula\Entities\TransactionRequest;
use Crassula\Entities\TransferCardToCard;
use Crassula\Entities\VerifyCardGetToken;
use Crassula\Entities\VerifyPayPage;
use Crassula\Exceptions\CrassulaException;

final class Client
{
    use Signatures;

    private $httpClient;
    private $config;

    /**
     * @param CrassulaConfig $config
     * @throws CrassulaException
     */
    public function __construct(CrassulaConfig $config, bool $logging = true)
    {
        self::validateConfig($config);
        $this->config = $config;
        $this->httpClient = new HttpClient();
        $this->httpClient->withLogging($logging);
    }

    /**
     * @return HttpClient
     */
    public function getHttpClient(): HttpClient
    {
        return $this->httpClient;
    }

    /**
     * @return CrassulaConfig
     */
    public function getConfig(): CrassulaConfig
    {
        return $this->config;
    }

    // services

    public function getPaymentService(): PaymentService
    {
        return new PaymentService($this);
    }

    public function getTransactionService(): TransactionService
    {
        return new TransactionService($this);
    }

    public function getVerifyService(): VerifyService
    {
        return new VerifyService($this);
    }

    public function getTransferService(): TransferService
    {
        return new TransferService($this);
    }

    public function getCreditService(): CreditService
    {
        return new CreditService($this);
    }

    public function getCheckingAccountService(): CheckingAccountService
    {
        return new CheckingAccountService($this);
    }

    public function getInvoiceService(): InvoiceService
    {
        return new InvoiceService($this);
    }

    public function getCurrencyService(): CurrencyService
    {
        return new CurrencyService($this);
    }

    /**
     * @deprecated
     * */
    public function getMerchantService(): MerchantService
    {
        return new MerchantService($this);
    }

    // alias methods of services

    /**
     * @param Purchase $purchase
     * @param bool $offline
     * @return \Crassula\Entities\PurchaseResult|string|null
     * @throws CrassulaException
     */
    public function purchase(Purchase $purchase, bool $offline = true)
    {
        return self::getPaymentService()->purchase($purchase, $offline);
    }

    /**
     * @param Settle $settle
     * @return \Crassula\Entities\SettleResult
     * @throws CrassulaException
     */
    public function settle(Settle $settle)
    {
        return self::getPaymentService()->settle($settle);
    }

    /**
     * @param Charge $charge
     * @return \Crassula\Entities\ChargeResult
     * @throws CrassulaException
     */
    public function charge(Charge $charge)
    {
        return self::getPaymentService()->charge($charge);
    }

    /**
     * @param CheckStatus $checkStatus
     * @return \Crassula\Entities\CheckStatusResult
     * @throws CrassulaException
     */
    public function checkStatus(CheckStatus $checkStatus)
    {
        return self::getPaymentService()->checkStatus($checkStatus);
    }

    /**
     * @param Refund $refund
     * @return \Crassula\Entities\RefundResult
     * @throws CrassulaException
     */
    public function refund(Refund $refund)
    {
        return self::getPaymentService()->refund($refund);
    }

    /**
     * @param CreateInvoice $createInvoice
     * @return \Crassula\Entities\CreateInvoiceResult
     * @throws CrassulaException
     */
    public function createInvoice(CreateInvoice $createInvoice)
    {
        return self::getInvoiceService()->create($createInvoice);
    }

    /**
     * @param RemoveInvoice $removeInvoice
     * @return \Crassula\Entities\RemoveInvoiceResult
     * @throws CrassulaException
     */
    public function removeInvoice(RemoveInvoice $removeInvoice)
    {
        return self::getInvoiceService()->remove($removeInvoice);
    }

    /**
     * @param VerifyPayPage $verifyPayPage
     * @return string|null
     * @throws CrassulaException
     */
    public function verifyInPayPage(VerifyPayPage $verifyPayPage)
    {
        return self::getVerifyService()->verifyInPayPage($verifyPayPage);
    }

    /**
     * @param VerifyCardGetToken $verifyCardGetToken
     * @return \Crassula\Entities\VerifyCardGetTokenResult
     * @throws CrassulaException
     */
    public function verifyCardGetToken(VerifyCardGetToken $verifyCardGetToken)
    {
        return self::getVerifyService()->verifyCardGetToken($verifyCardGetToken);
    }

    /**
     * @param TransferCardToCard $transferCardToCard
     * @return \Crassula\Entities\TransferCardToCardResult
     * @throws CrassulaException
     */
    public function transferCardToCard(TransferCardToCard $transferCardToCard)
    {
        return self::getTransferService()->transfer($transferCardToCard);
    }

    /**
     * @param CreditCard $creditCard
     * @return \Crassula\Entities\CreditCardResult
     * @throws CrassulaException
     */
    public function creditCard(CreditCard $creditCard)
    {
        return self::getCreditService()->credit($creditCard);
    }

    /**
     * @param CheckingAccount $checkingAccount
     * @return \Crassula\Entities\CheckingAccountResult
     * @throws CrassulaException
     */
    public function fillCheckingAccount(CheckingAccount $checkingAccount)
    {
        return self::getCheckingAccountService()->fillAccount($checkingAccount);
    }

    /**
     * @param TransactionRequest $request
     * @return \Crassula\Entities\TransactionResult
     * @throws CrassulaException
     */
    public function transactionList(TransactionRequest $request)
    {
        return self::getTransactionService()->list($request);
    }


    /**
     * @param array $data
     * @return string
     * @throws CrassulaException
     */
    public function signature(array $data): string
    {
        return self::doSignature($data, $this->getConfig()->getMerchantSecretKey());
    }

    /**
     * @throws CrassulaException
     */
    protected static function validateConfig(CrassulaConfig $config)
    {
        if (empty($config->getMerchantAccount())) {
            throw new CrassulaException('Config [MerchantAccount] is required');
        }

        $key = $config->getMerchantSecretKey();

        if (!isset($key)) {
            throw new CrassulaException('Config [MerchantSecretKey] is present');
        }
    }
}
